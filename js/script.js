jQuery(document).ready(function() {
    var slider = jQuery('.px_bxslider').bxSlider({
        auto: false,
        mode: 'fade',
        controls: false,
        onSliderLoad: function(currentIndex) {
            jQuery(".px_slideshow .slide_1").addClass('slideanimate');
        }
        // onSlideBefore: function(slideElement, oldIndex, newIndex) {
        //     jQuery(".px_slideshow .slideanimate").removeClass('slideanimate');
        //     slideElement.addClass('slideanimate');
        // }
    });

    
    var navHeight = jQuery('.px_header_nav').height();
    jQuery('.btn-navbar').click(function() {
        jQuery('body').toggleClass('extend');
        jQuery(".px_header_nav").toggleClass("colapsed");
        return false;

    });

    // Typing Animation
    var i = 0,
        a = 0,
        isBackspacing = false;
    var textArray = [
      "brands", 
      "experiences", 
      "solutions"
    ];
    //Run the loop
    typeWriter("bannertext", textArray);

    function typeWriter(id, ar) {
      var element = jQuery("#" + id),
          aString = ar[a],
          eHeader = element.children().find("span");
          
        if(eHeader.text().length == aString.length){
              isBackspacing = true;
              setTimeout(function(){ eHeader.text(eHeader.text().substring(0, eHeader.text().length - 1)); typeWriter(id, ar); }, 2000);
        }else{
          if (!isBackspacing) {
            if (i < aString.length) {
              if (aString.charAt(i) == "|") {
                i++;
                setTimeout(function(){ typeWriter(id, ar); }, 99);
              } else {
                eHeader.text(eHeader.text() + aString.charAt(i));
                i++;
                //eHeader.addClass("textchange"+aString.length);
                setTimeout(function(){ typeWriter(id, ar);}, 200);
              }
            } else if (i == aString.length) {    
              isBackspacing = true;
              setTimeout(function(){ typeWriter(id, ar); }, 2000);
            }
          // If backspacing is enabled
          } else {
              if (eHeader.text().length > 0) {
                eHeader.text(eHeader.text().substring(0, eHeader.text().length - 1));
                setTimeout(function(){ typeWriter(id, ar); }, 39);
              }
              else { 
              eHeader.addClass('textchange');
              isBackspacing = false;
              i = 0;
              a = (a + 1) % ar.length;
              setTimeout(function(){ typeWriter(id, ar); }, 50);
            }
          }
      }
    }
    /* contact and request form function*/
  
 
  jQuery(".coloum_btn_wrapper #request,.header-request").click(function(e){
    showRequestForm();
  });
  if(window.location.hash == '#request'){
      console.log("request")
      showRequestForm();
  }
 
  jQuery(".coloum_btn_wrapper #contact").click(function(e){
    showContactForm();
  });

  if(window.location.hash == '#contact'){
    console.log("contact")
    showContactForm();
  }
  function showRequestForm(){
    jQuery("#form_contact").hide();
    jQuery("#form_request").show();
    jQuery(".px_whitebg_quote").removeClass('contact_active');
    jQuery(".px_whitebg_quote").addClass('request_active');
  }
  function showContactForm(){
    jQuery("#form_request").hide();
    jQuery("#form_contact").show();
    jQuery(".px_whitebg_quote").removeClass('request_active');
    jQuery(".px_whitebg_quote").addClass('contact_active');
   }
    
});


               