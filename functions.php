<?php
/**
 * pixel functions and definitions
 *
 * @package pixel
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
	include('library/includes.php');
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'pixel_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function pixel_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on pixel, use a find and replace
	 * to change 'pixel' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'pixel', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'pixel' ),
		'footerArray' => __( 'footer array', 'pixel' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
	add_theme_support( 'post-thumbnails' );
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'pixel_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // pixel_setup
add_action( 'after_setup_theme', 'pixel_setup' );
/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function pixel_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'pixel' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'pixel_widgets_init' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


class Menu_With_Description extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth = 0, $args = [], $id = 0) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<small class="sub">' . $item->description . '</small>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}


$page_meta_boxes= array( "theme_page_title" => array(
                            "name" => "theme_page_title",
                            "std" => '',
                            "title" => "Page Title",
                            "description" => "",
                            "type" => "text",
                            "class" => ""),
							"theme_page_subtitle" => array(
								"name" => "theme_page_subtitle",
								"std" => '',
								"title" => "Page Sub Title",
								"description" => "",
								"type" => "text",
								"class" => "")
							);	
	theme_create_post_type('page', '', $page_meta_boxes);

	
	$post_meta_boxes= array(
							"theme_post_subtitle" => array(
								"name" => "theme_post_subtitle",
								"std" => '',
								"title" => "Post Sub Title",
								"description" => "",
								"type" => "text",
								"class" => "")
							);	
							theme_create_post_type('post', '', $post_meta_boxes);								
											

	add_filter('the_excerpt', 'theme_excerpt');

function theme_excerpt($content){
return wp_trim_words( strip_shortcodes($content), $num_words = 25, $more = null ); 
}
function reg_tag() {
     register_taxonomy_for_object_type('post_tag', 'portfolio');
}
add_action('init', 'reg_tag');

function thinkoverit_widgets_init() {
register_sidebar( array(
		'name'          => __( 'Footer	social Sidebar', 'pixel' ),
		'id'            => 'footer-social-sidebar',
  'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',) );
}
add_action( 'widgets_init', 'thinkoverit_widgets_init' );

function thinkoverit_tweet_widgets_init() {
register_sidebar( array(
		'name'          => __( 'Footer Tweet Sidebar', 'tweet' ),
		'id'            => 'footer-tweet-sidebar',
  'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',) );
}
add_action( 'widgets_init', 'thinkoverit_tweet_widgets_init' );


  $labels = array(
		'name'               => _x( 'Portfolio', 'post type general name' ),
		'singular_name'      => _x( 'Portfolio', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Portfolio' ),
		'add_new_item'       => __( 'Add New Portfolio' ),
		'edit_item'          => __( 'Edit Portfolio' ),
		'new_item'           => __( 'New Portfolio' ),
		'all_items'          => __( 'All Portfolio' ),
		'view_item'          => __( 'View Portfolio' ),
		'search_items'       => __( 'Search Portfolio' ),
		'not_found'          => __( 'No Portfolio found' ),
		'not_found_in_trash' => __( 'No Portfolio found in the Trash' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Portfolio'
	);
 $args = array(
		'labels'        => $labels,
		'description'   => 'Portfolio',
		'public'        => true,
		'menu_position' => 8,
		'supports'      => array( 'title', 'editor', 'thumbnail'),
		'has_archive'   => false,
		'rewrite'		=>array('slug'  => 'portfolio'),
	);
$taxlabels = array(
		'name'                       => _x( 'Portfolio Categories', 'taxonomy general name' ),
		'singular_name'              => _x( 'Portfolio Categories', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Categories' ),
		'popular_items'              => __( 'Popular Categories' ),
		'all_items'                  => __( 'All Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category' ),
		'update_item'                => __( 'Update Category' ),
		'add_new_item'               => __( 'Add New Category' ),
		'new_item_name'              => __( 'New Category Name' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Category' ),
		'choose_from_most_used'      => __( 'Choose from the most used Categories' ),
		'not_found'                  => __( 'No Categories found.' ),
		'menu_name'                  => __( 'Portfolio Categories' ),
	);

	$taxargs = array(
		'hierarchical'          => true,
		'labels'                => $taxlabels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'portfolio-category' ),
	);
 
 $portfolio_taxonomy = array('portfolio_category'=> $taxargs);
 
 $portfolioLink = array(
                    "portfolioLink" => array(
                             "name" => "portfolioLink",
                             "std" => '',
                             "title" => "Visit Project",
                             "description" => "",
                             "type" => "text",
                             "multiple"=> false,
                             "class" => ""),
                    );



 theme_create_post_type('portfolio', $args,  $portfolioLink , $portfolio_taxonomy);
	

	
	
function wp_pagination() {
global $wp_query;
$big = 999999;
$page_format = paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $wp_query->max_num_pages,
    'type'  => 'array'
) );
if( is_array($page_format) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<div><ul class="px_page_nav">';
           
            foreach ( $page_format as $page ) {
                    echo "<li>$page</li>";
            }
           echo '</ul></div>';
}
}

	

	$labels = array(
		'name'               => _x( 'Team', 'post type general name' ),
		'singular_name'      => _x( 'Team', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
		'add_new_item'       => __( 'Add New Team' ),
		'edit_item'          => __( 'Edit Team' ),
		'new_item'           => __( 'New Team' ),
		'all_items'          => __( 'All Team' ),
		'view_item'          => __( 'View Team' ),
		'search_items'       => __( 'Search Team' ),
		'not_found'          => __( 'No Team found' ),
		'not_found_in_trash' => __( 'No Team found in the Trash' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Team'
	);

 $args = array(
		'labels'        => $labels,
		'description'   => 'Team',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail'),
		'has_archive'   => false,
		'rewrite'		=>array('slug'  => 'team'),
	);
 
$taxlabels = array(
		'name'                       => _x( 'Team Categories', 'taxonomy general name' ),
		'singular_name'              => _x( 'Team Categories', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Categories' ),
		'popular_items'              => __( 'Popular Categories' ),
		'all_items'                  => __( 'All Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category' ),
		'update_item'                => __( 'Update Category' ),
		'add_new_item'               => __( 'Add New Category' ),
		'new_item_name'              => __( 'New Category Name' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Category' ),
		'choose_from_most_used'      => __( 'Choose from the most used Categories' ),
		'not_found'                  => __( 'No Categories found.' ),
		'menu_name'                  => __( 'Team Categories' ),
	);

	$taxargs = array(
		'hierarchical'          => true,
		'labels'                => $taxlabels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'		=>array('slug'  => 'team-category'),
	);
 
 $team_taxonomy = array('team_category'=> $taxargs);

 $itemclass = array(
                    "itemclass" => array(
                             "name" => "itemclass",
                             "std" => '',
                             "title" => "Class",
                             "description" => "",
                             "type" => "text",
                             "multiple"=> false,
                             "class" => ""),
                    );



 theme_create_post_type('team', $args, $itemclass , $team_taxonomy);
	

add_action( 'template_redirect', 'wpse_128636_redirect_post' );

function wpse_128636_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'team' ==  $queried_post_type ) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}

$client_labels = array(
	'name'               => _x( 'Client', 'post type general name' ),
	'singular_name'      => _x( 'Client', 'post type singular name' ),
	'add_new'            => _x( 'Add New', 'Client' ),
	'add_new_item'       => __( 'Add New Client' ),
	'edit_item'          => __( 'Edit Client' ),
	'new_item'           => __( 'New Client' ),
	'all_items'          => __( 'All Client' ),
	'view_item'          => __( 'View Client' ),
	'search_items'       => __( 'Search Client' ),
	'not_found'          => __( 'No Client found' ),
	'not_found_in_trash' => __( 'No Client found in the Trash' ), 
	'parent_item_colon'  => '',
	'menu_name'          => 'Client'
);
$client_args = array(
	'labels'        => $client_labels,
	'description'   => 'Client',
	'public'        => true,
	'menu_position' => 8,
	'supports'      => array( 'title', 'editor', 'thumbnail'),
	'has_archive'   => true,
	'rewrite'		=>array('slug'  => 'client'),
);

theme_create_post_type('client', $client_args);

add_filter('deprecated_constructor_trigger_error', '__return_false');

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'acme_quote',
    array(
      'labels' => array(
        'name' => __( 'Quotes' ),
        'singular_name' => __( 'Quote' ),

      ),
      'supports'      => array( 'title', 'editor', 'thumbnail'),
      'public' => true,
      'has_archive' => true,
    )
  );
}

//custom post type of career
// career Custom Post Type
function career_init() {
    // set up Career labels
    $labels = array(
        'name' => 'Careers',
        'singular_name' => 'Career',
        'add_new' => 'Add New Career',
        'add_new_item' => 'Add New Career',
        'edit_item' => 'Edit Career',
        'new_item' => 'New Career',
        'all_items' => 'All Careers',
        'view_item' => 'View Career',
        'search_items' => 'Search Careers',
        'not_found' =>  'No Careers Found',
        'not_found_in_trash' => 'No Careers found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Careers',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => false,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'career'),
        'query_var' => true,
        'menu_icon' => 'dashicons-randomize',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'custom-fields',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'career', $args );
    
    // register taxonomy
    register_taxonomy('career_category', 'career', array('hierarchical' => true, 'label' => 'Category', 'query_var' => true, 'rewrite' => array( 'slug' => 'career-category' )));
}
add_action( 'init', 'career_init' );



//Rewrite profile page link from team to about-us-and-services
function add_custom_rewrite_rule() {
    if( ($current_rules = get_option('rewrite_rules')) ) {
        foreach($current_rules as $key => $val) {
            if(strpos($key, 'team') !== false) {
                add_rewrite_rule(str_ireplace('team', 'about-us-and-services', $key), $val, 'top');   
            } 
        } 
    }
    flush_rewrite_rules();
} 
add_action('init', 'add_custom_rewrite_rule');
/**
 * Enqueue scripts and styles.
 */
function pixel_scripts() {
	wp_enqueue_style( 'pixel-style', get_stylesheet_uri() );
    wp_enqueue_style('reset-css', get_bloginfo('stylesheet_directory'). '/css/reset.css');
   	wp_enqueue_style('common-css', get_bloginfo('stylesheet_directory'). '/css/common.min.css');
  	wp_enqueue_script('pixel_script_masonry', get_bloginfo('stylesheet_directory'). '/js/masonry.pkgd.min.js');
   	wp_enqueue_script('bx_slider',get_template_directory_uri(). '/js/jquery.bxslider.js', array('jquery') ,false, true);
   	wp_enqueue_script('script', get_template_directory_uri(). '/js/imagesloaded.js', array('jquery'), false,true);	
	wp_enqueue_script('custom-script', get_template_directory_uri(). '/js/script-min.js', array('jquery'), false,true);	
}
add_action( 'wp_enqueue_scripts', 'pixel_scripts' );

register_sidebar( array(
		'name'          => __( 'Site Map', 'pixel' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
) );
/* adding Meta Description*/
function pixel6_meta_description() {
    global $post;
    if ( is_singular() && $post->post_content!="" ) {
        $des_post = strip_tags( $post->post_content );
        $des_post = strip_shortcodes( $des_post );
        $des_post = str_replace( array("\n", "\r", "\t", "\""), ' ', $des_post );
        $des_post = mb_substr( $des_post, 0, 300, 'utf8' );
        echo '<meta name="description" content="' . $des_post . '" />' . "\n";
    }
    elseif ( is_home() ) {
        echo '<meta name="description" content="' . get_bloginfo( "description" ) . '" />' . "\n";
    }
    elseif ( is_category() ) {
        $des_cat = strip_tags(category_description());
        echo '<meta name="description" content="' . $des_cat . '" />' . "\n";
    }
	else{
		echo '<meta name="description" content="Pixel6 is an IT services, a UI/UX focused Front End Engineering, Web, Mobile Apps &amp; WordPress Development company based in Pune, India." />' . "\n";
	}
}
add_action( 'wp_head', 'pixel6_meta_description',1);



add_filter( 'wpcf7_validate_text', 'wpcs_custom_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_text*', 'wpcs_custom_validation_filter', 10, 2 );
 
function wpcs_custom_validation_filter( $result, $tag ) {
	$name = $tag->name;
 
	$value = isset( $_POST[$name] )
		? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
		: '';
 
	if ( 'text' == $tag->basetype ) {
		if ( preg_match('/\d/', $value ) ) {
			$result->invalidate( $tag, 'Sorry... Number exists..' );
//$result->invalidate( $tag, wpcf7_get_message( 'invalid_wpcs_custom_error' ) );
		}
	}
    return $result;
}
