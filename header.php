<?php
/**
* The header for our theme.
*
* Displays all of the <head> section and everything up till <div id="content">
	*
	* @package pixel
	*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>	
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:image" content="<?php bloginfo('template_directory'); ?>/logo.png">
		<meta property="og:image:type" content="image/png">
		<meta property="og:image:width" content="100">
		<meta property="og:image:height" content="100">
		<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/x-icon">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		
		<?php wp_head(); ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164690931-1"></script>
		<script>
			 window.dataLayer = window.dataLayer || [];
			 function gtag(){dataLayer.push(arguments);}
			 gtag('js', new Date());

			 gtag('config', 'UA-164690931-1');
		</script>

		<!--[if IE]>
		<script src="<?php //echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<?php //bloginfo('stylesheet_directory'); ?>/css/ie.css" type="text/css" media="screen, projection"><![endif]-->
	</head>
	<?php if(is_page()) { $page_slug = $post->post_name;} else $page_slug = "page"; ?>
	<body <?php body_class($page_slug); ?>>
		<section class="px_main_section clearfix">
			<div id="page" class="hfeed site">
				<header id="masthead" class="site-header"> <!--  role="banner" -->
					<div class="px_header">
						<div class="content_wrapper_width <?php if(!is_page("pixel6-and-covid19")){ ?> px_header_top <?php } ?>  clearfix">
							<a href="<?php echo home_url( '/' ); ?>" id="px_logo" class="px-logo align_left" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
							rel="home"><img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/logo.png" alt="<?php echo get_bloginfo( 'name' ); ?>"/></a>
							<a class="btn btn-navbar tnav-btn" data-target=".nav-collapse" >  Menu </a>
							<nav id="site-navigation" class="px_header_nav align_right"> <!-- role="navigation" -->
								<?php $walker = new Menu_With_Description; ?>
								<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' , 'walker' => $walker) ); ?>
							</nav><!-- #site-navigation -->
							
						</div>
						<?php $subtitle = get_post_meta($post->ID,'theme_page_subtitle_value', true); ?>
						<?php $title = get_post_meta($post->ID,'theme_page_title_value', true); ?>
						<?php if(is_home()) { ?>
							<div class="clearfix  px_header_bottom px_title_section content_wrapper_width">
								<h1>the blog.</h1>
								<h3>here we discuss design, branding and technology.</h3>
							</div>
						<?php }else if(is_404()){ ?>
							<div class="clearfix  px_header_bottom px_title_section content_wrapper_width">
								<h1>404!</h1>
								<h3>oops! looks like this page does not exist or is under construction. we apologize!</h3>
							</div>
						<?php }else if(is_page("pixel6-and-covid19")){ ?>
							<div class="clearfix  content_wrapper_width covid-banner">
								<!--<img src="http://pixel6.co/wp-content/uploads/2020/04/img-covid-19-new-e1587452400859.png" alt="Pixel6 &amp; Covid-19"/>-->
								<img src="https://pixel6.co/wp-content/uploads/2023/03/covid-v1-final.jpg" alt="Pixel6 &amp; Covid-19"/>
							</div>
						<?php }else if(is_search()){ ?>
							<div class="clearfix  px_header_bottom px_title_section content_wrapper_width">
								<h1> <?php echo  __( 'Search Results' ); ?></h1>
								<h3><?php printf( __( 'for: "%s"' ),  get_search_query()); ?></h3>
							</div>
						<?php }else if(is_author()){ ?>
							<div class="clearfix  px_header_bottom px_title_section content_wrapper_width">
								<h1>  <?php echo  __( 'Author Archives' ); ?></h1>
								<h3><?php echo get_the_author();?></h3>
							</div>
						<?php } else if(is_front_page()){ ?>
							<?php if(!empty($title)) { ?>
								<div class="clearfix  px_header_bottom px_title_section content_wrapper_width" id="bannertext">
									<h1 class="page-title">we build <span class="textchange">brands</span></h1>
									<h3><?php echo $subtitle; ?></h3>
								</div>
							<?php }?>
						<?php } else{?>

						<?php if(!empty($title)) { ?>
							<div class="clearfix  px_header_bottom px_title_section content_wrapper_width">
								<h1 class="page-title"><?php echo $title; ?></h1>
								<h3><?php echo $subtitle; ?></h3>
							</div>
						<?php } }?>
						<?php if(is_single()){ ?>
							<div class="clearfix  px_header_bottom px_title_section content_wrapper_width">
								<h1 class="page-title"><?php the_title(); ?></h1>
								<a href="<?php echo home_url( '/' ); ?>" class="author_name"><span>by</span> pixel6 <span>on <?php echo get_the_date();?></span></a>
							</div>
						<?php }?>
						
					</div>
				</header><!-- #masthead -->
				<div id="content" class="site-content">