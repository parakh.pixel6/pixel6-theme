<?php

class toitmap extends WP_Widget {
	public function __construct() {
		// widget actual processes
		parent::__construct(
			'toit-map-widget',
			__( 'Toit map '),
			array( 'description' => __( 'Displays map ') ),
			array( 'width' => 400,
				   'height' => 350 )
		);
	}
	function parse_instance_args( $instance ) {
		$instance = wp_parse_args( (array)$instance,
			array(
				'title'=>'',
                'address'=>'',
			)
		);
		return $instance;
	}

public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );	
        $instance['address'] = strip_tags( $new_instance['address'] );
        $instance['show_map'] = $new_instance['show_map'];
		return $instance;
	}
    
 public function widget( $args, $instance ) {

   global $wp_query;

   $instance = $this->parse_instance_args( $instance );
   $instance['title'] = apply_filters( 'widget_title', $instance['title'] );		
   echo $args['before_widget'];
?>
		 <div class="toit-widget-map">
					<?php if(!empty($instance['title'])){ ?>
						<h1><?php echo $instance['before_title']. $instance['title']. $instance['after_title']; ?></h1>
					<?php } ?>	
                   
					<?php if($instance['show_map']){ ?>
      <iframe src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo urlencode($instance['address']); ?>&amp;ie=UTF8&amp;t=m&amp;z=16&amp;output=embed"></iframe>                    
     <?php } ?>
   </div>
<?php		  
		  echo $args['after_widget'];
	}  
public function form( $instance ) {
		$plugin_dir = plugin_dir_url( __FILE__ );
		$instance = $this->parse_instance_args( $instance );
?>
 <div class="toit-widget-map-form">
  <ul class="toit-widget-map-fromul">
			<li>
				<label for="<?php print $this->get_field_id( 'title' ); ?>"><?php print __( 'Title:' ); ?></label>
				<input id="<?php print $this->get_field_id( 'title' ); ?>" name="<?php print $this->get_field_name( 'title' ); ?>" value="<?php print $instance['title']; ?>" type="text">
			</li>
	        <li>
            <label
                for="<?php print $this->get_field_id( 'address' ); ?>"><?php print __( 'Enter Address: ' ); ?></label>
              <textarea  name="<?php print $this->get_field_name( 'address' ); ?>" id="<?php print $this->get_field_id( 'address' ); ?>"> <?php print $instance['address']; ?> </textarea>      
            </li>
            <li>
		  <label
			 for="<?php print $this->get_field_id( 'show_map' ); ?>"><?php print __( 'Show Map: ' ); ?></label>
		    <input type="checkbox" size="3" value="1"
				<?php if($instance['show_map']) echo 'checked="checked"';?>
			   name="<?php print $this->get_field_name( 'show_map' );?>" id="<?php print $this->get_field_id( 'show_map' ); ?>">
          </li>
           
   </ul>
   
</div>

<?php 	
	}
}
function toit_map_init(){
     register_widget( 'toitmap' );
}
add_action( 'widgets_init', 'toit_map_init');
?>
