<?php

class toitnewsletterwidget extends WP_Widget {
    
 var $error = '';
 var $success='';
	public function __construct() {
		// widget actual processes
        
		parent::__construct(
			'theme-newsletter-widget',
			__( 'Toit Newsletter  Widget'),
			array( 'description' => __( 'Allows Email Subscription.') ),
			array( 'width' => 400,
				  'height' => 350 )           
		);
  if ( is_active_widget( false, false, $this->id_base ) ) {
    $this->create_table();
   
    add_action( 'init', array( $this, 'toitnewsletter_add_email_subscription' ) ); 
    
    add_action( 'admin_menu', array( $this, 'toitnewsletter_subscriber_page' ) );
  }
	}
 function toitnewsletter_subscriber_page(){
  add_menu_page( 'Newsletter Subscribers', 'Newsletter Subscribers', 'administrator', 'toitnewsletterwidget_subscriber', array( $this, 'render_toitnewsletterwidget_subscriber' ) );
 }
    
 private function get_list_html($per_page, $pageno){
  global $wpdb;

  $table_name = $this->get_newsletter_table_name();
      
  $offset = $per_page * ( $pageno - 1);
  
  if(!$pageno && !$per_page){
      $results=$wpdb->get_results( "SELECT * FROM $table_name " ); 
  }else{
      $totalrecord = $wpdb->get_var( "SELECT COUNT(*) FROM $table_name ");
      $results=$wpdb->get_results( "SELECT * FROM $table_name LIMIT $offset, $per_page "  ); 
  }

  $htm .= '<table border="1" class="subscription-table" width="100%">';
  $htm .= '<thead>';
  $htm .= '<tr>';
  $htm .= '<th>Id</th>';
  $htm .= '<th>Email</th>';
  $htm .= '</tr>';
  $htm .= ' </thead>'; 
  $htm .= '<tbody>';
  
  foreach($results as $result){
      $htm .= '<tr>';
      $htm .= '<td> '.$result->sub_id.'</td>' ;
      $htm .= '<td> '.$result->email.'</td>' ; 
      $htm .= '</tr>';
  }
     
  $htm .= '</tbody>';
  $htm .= '</table>';

  if(!$pageno && !$per_page){
   $pages_count = (int) $totalrecord/$per_page ;
   if($totalrecord % $per_page) $pages_count++;
   
   if($pageno>1){
       $htm .= '<a href="'.$this->get_subscriber_page_url().'&pageno='.($pageno - 1).'">'. '<h2 class="prev-page">Prev</h2>' .'</a>';
   }if($pageno < $pages_count){
      $htm .= '<a href="'.$this->get_subscriber_page_url().'&pageno='.($pageno + 1).'">'. '<h2 class="next-page">Next</h2>' .'</a>';    
   }
  }
  return $htm;
 }
    
 function render_toitnewsletterwidget_subscriber(){

 $per_page = 10;
?>   
     <div class="toit-widget-newsletter-subscriber-list">
         <h2 class="head-newsletter">Newsletter Subscriber List</h2>
         <a href="<?php echo $this->get_subscriber_page_url(); ?>&export=true" class="export-data" >Export</a>
<?php 
         $pageno = (isset($_GET['pageno']) && is_numeric($_GET['pageno'])) ? $_GET['pageno'] : 1 ;
         echo $this->get_list_html($per_page, $pageno);
?>
     </div>
<?php
 }

 private function get_subscriber_page_url(){
   return admin_url('admin.php?page=toitnewsletterwidget_subscriber'); 
 }
 private function get_newsletter_table_name(){
   return "toit_newsletter"; 
 }
 private function check_newsletter_table_exists() {
   global $wpdb;
   $table_name = $this->get_newsletter_table_name();
   return strtolower( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) ) == strtolower( $table_name );
 }
 private function create_table() {
  global $wpdb;

  if ( $this->check_newsletter_table_exists() )
      return; //  already exists

  $table_name = $this->get_newsletter_table_name();

  $ret = $wpdb->query( "CREATE TABLE IF NOT EXISTS $table_name (
          sub_id bigint(20) unsigned NOT NULL auto_increment,
          email varchar(140) NOT NULL default '',
          PRIMARY KEY (sub_id));" );

  if ( $ret === false )
      return false;
  return TRUE;
 }
 
 function toitnewsletter_add_email_subscription($email) {
  global $wpdb;
  //Widget Form save 
  if (isset($_POST['toitnewsletterwidget_submit_form']) ){
      $email = $_POST['toitnewsletterwidget_email'];
      if(!is_email($email)){
          $this->error = "Please enter valid email";
      }
      if(is_email($email)){
       $this->success = "Email Subscription Successful";
      }
      $exists = $wpdb->get_var( 'SELECT COUNT(*) FROM toit_newsletter  WHERE email = "'.$email.'"' );

      if (!$exists){
          $result= $wpdb->insert('toit_newsletter', array('email' => $email));
      }
  }
  //Export list 
  if( (is_admin() && isset($_GET['page']) && $_GET['page'] == 'toitnewsletterwidget_subscriber') && isset($_GET['export']) && $_GET['export'] == true){
       header("Content-type: application/octet-stream");
       header("Content-Disposition: attachment; filename=users.xls");
       header("Pragma: no-cache");
       echo $this->get_list_html(1, 0);
       die;   
  }
  return true;
 }
	private function parse_instance_args( $instance ) {
		$instance = wp_parse_args( (array)$instance,
			array(
				'title' => '',		
			)
		);
		return $instance;
	}

 public function widget( $args, $instance ) {

        $instance = $this->parse_instance_args( $instance ); 
        $instance['title'] = apply_filters( 'widget_title', $instance['title'] );
        
        echo $args['before_widget'];
?>
   <div class="toit-widget-newsletter-form">
     <h3 class="toit-widget-newsletter-title"><?php echo   $instance['title'] ; ?></h3>
     <form method="POST"  action="">
       <ul class="toit-widget-newsletter-ul">
          <li><input class="toit-widget-newsletter-email" type="email"  value="" id="toitnewsletterwidget_email" name="toitnewsletterwidget_email" ></li>
          <li><input class="toit-widget-newsletter-submit" type="submit" value="Subscribe" id="toitnewsletterwidget_submit_form" name="toitnewsletterwidget_submit_form"></li>
       </ul>
     </form>
     <?php if(!empty($this->error)) { ?>
        <p class="toit-widget-newsletter-error"><?php echo __($this->error); ?></p>
     <?php }  ?>
     <?php if(!empty($this->success)) { ?>
        <p class="toit-widget-newsletter-success"><?php echo __($this->success); ?></p>
     <?php }  ?>
   </div>
   
<?php
  echo $args['after_widget'];
 }

 public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
 }
	
 public function form( $instance ) {

		$instance = $this->parse_instance_args( $instance );
		$title = strip_tags( $instance['title'] );

?>
<div class="toit-widget-form">
  <ul class="toit-widget-fromul">
	<li>
		<label for="<?php print $this->get_field_id( 'title' ); ?>"><?php print __( 'Title: ' ); ?>:</label>
		<input class="widefat" id="<?php print $this->get_field_id( 'title' ); ?>"
		   name="<?php print $this->get_field_name( 'title' ); ?>" value="<?php print $title; ?>" type="text">
    </li>
   </ul>
</div>
<?php 	
 }
}

function theme_newsletter_widget_init(){
     register_widget( 'toitnewsletterwidget' );
}
add_action( 'widgets_init', 'theme_newsletter_widget_init');

