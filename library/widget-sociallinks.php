<?php

class toitsociallink extends WP_Widget {

	var $variables = 		array( 

						"Facebook" => array(

						   "fbtitle" => array('type'=>'text','title'=> "Title"),

						   "fblink" => array('type'=>'text','title'=> "Link"),

						   "fbclass" => array('type'=>'text','title'=> "Class"),

									"fborder" => array('type'=>'select','title'=> "Order")),

					  "Twitter" => array(

						   "twtitle" => array('type'=>'text','title'=> "Title"),

						   "twlink" => array('type'=>'text','title'=> "Link"),

						   "twclass" => array('type'=>'text','title'=> "Class"),

									"tworder" => array('type'=>'select','title'=> "Order")),   

					  "Linkedin" => array(

						   "lntitle" => array('type'=>'text','title'=> "Title"),

						   "lnlink" => array('type'=>'text','title'=> "Link"),

						   "lnclass" => array('type'=>'text','title'=> "Class"),

									"lnorder" => array('type'=>'select','title'=> "Order")),

							"Trumbler" => array(

						   "trtitle" => array('type'=>'text','title'=> "Title"),

						   "trlink" => array('type'=>'text','title'=> "Link"),

						   "trclass" => array('type'=>'text','title'=> "Class"),

									"trorder" => array('type'=>'select','title'=> "Order")),

      

       "RSS" => array(

         "rstitle" => array('type'=>'text','title'=> "Title"),

         "rslink" => array('type'=>'text','title'=> "Link"),

         "rsclass" => array('type'=>'text','title'=> "Class"),

         "rsorder" => array('type'=>'select','title'=> "Order"))									

							);

	public function __construct() {

		// widget actual processes

		parent::__construct(

			'toit-social-link-widget',

			__( 'Toit social link'),

			array( 'description' => __( 'Displays Social Link ') ),

			array( 'width' => 400,

				'height' => 350 )

		);

	}

function parse_instance_args( $instance ) {

		$instance = wp_parse_args( (array)$instance,

			array(

			 'title' =>'',

    'window' => 'new-window',

			)

		);

		return $instance;

	}

public function update( $new_instance, $old_instance ) {



	$instance = $old_instance;

	$instance['window'] = $new_instance['window'];

		foreach($this->variables as $index=>$options){

			foreach($options as $field=>$props){

										switch($props['type'])

										{

												case 'select':

													$instance[$field] =  $new_instance[$field];

												break;

												case 'text':

														$instance[$field] = strip_tags( $new_instance[$field] );

												break;

												case 'images':

															$instance[$field] = $new_instance[$field];

												break;

			       }

		 }

	 }

		return $instance;

}

	public function widget( $args, $instance ) {

		// outputs the content of the widget

		global $wp_query;

		extract( $args );

		$instance = $this->parse_instance_args( $instance );

		$instance['title'] = apply_filters( 'widget_title', $instance['title'] );		

  echo $args['before_widget'];

?>

		 <div class="toit-social-link-widget">

					<?php if(!empty($instance['title'])){ ?>

						<h1><?php echo $instance['title'] ; ?></h1>

					<?php }

							$arrayValues = array();

							foreach($this->variables as $index=>$options){

										$vars = array_keys($options);

										$arrayValues[$instance[$vars[3]]] = $options;

							}

							//uksort($arrayValues, 'usort_social_menucmp');

							foreach($arrayValues as $index=>$options){

								$vars = array_keys($options);

								$varsValues = array_values($options);

															switch($varsValues[0]['type'])

															{

																	case 'text':

?>

					<?php if(!empty($instance[$vars[1]])) { ?>

						<a href="<?php echo $instance[$vars[1]] ; ?>" <?php  if($instance['window']=='new-window'){ ?> target="_blank" <?php } ?> class="toit-widget-link <?php echo $instance[$vars[2]] ; ?>"><span><?php echo $instance[$vars[0]] ; ?></span></a>

					<?php } ?>	

<?php

																	break;		

               }

       }

?>

		 </div>

<?php		  

		  echo $args['after_widget'];

	}

	public function form( $instance ) {

		$instance = $this->parse_instance_args( $instance );

?>

	<div class="toit-widget-form">

  <ul class="toit-widget-fromul">

				<li>									

													<label for="<?php print $this->get_field_id( 'window' ); ?>"><?php print __( 'Open In Window' ); ?></label>

														<select id="<?php print $this->get_field_id( 'window' ); ?>" name="<?php print $this->get_field_name( 'window' ); ?>">

																<option value="same-window" <?php if($instance['window'] == 'same-window') echo 'selected="selected"'; ?>><?php print __( 'Same Window' ); ?></option>

																<option value="new-window" <?php if($instance['window'] == 'new-window') echo 'selected="selected"'; ?>><?php print __( 'New Window' ); ?></option>

														</select>		

					</li>

			<?php

							foreach($this->variables as $index=>$options){	

?>

												<li>

													<h3><?php print __( $index ); ?></h3>

<?php      

									foreach($options as $field=>$props) {

										switch($props['type'])

										{

												case 'select':

?>

													<label for="<?php print $this->get_field_id( $field ); ?>"><?php print __( $props['title'] ); ?></label>

														<select id="<?php print $this->get_field_id( $field ); ?>" name="<?php print $this->get_field_name( $field ); ?>">

               	<option value=""><?php echo "-select-"; ?></option>

<?php     

													for($i=1; $i <= count($this->variables); $i++) {

?> 

																<option value="<?Php echo $i; ?>" <?php if(isset($instance[$field]) && $instance[$field] == $i) echo 'selected="selected"'; ?>><?php print $i; ?></option>

<?php } ?>

														</select>	

<?php

												break;

												case 'text':

?>

													<label for="<?php print $this->get_field_id( $field ); ?>"><?php print __( $props['title'] ); ?></label>

													<input id="<?php print $this->get_field_id( $field ); ?>" name="<?php print $this->get_field_name( $field ); ?>" value="<?php if(isset($instance[$field])) print $instance[$field];?>" type="text" />

<?php

												break;

										}

								}

?>

								</li>

<?php

							}

?>

		</ul>

</div>

<?php 	

	}

}

function toit_widgets_init(){

     register_widget( 'toitsociallink' );

}

add_action( 'widgets_init', 'toit_widgets_init');

function usort_social_menucmp($a , $b){

 return $a - $b; 

}