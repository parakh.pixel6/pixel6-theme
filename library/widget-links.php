<?php
class toitlinkswidget extends WP_Widget {
    
	public function __construct() {
        
		parent::__construct(
			'theme-linksites-widget',
			__( 'Toit Links Widget'),
			array( 'description' => __( 'Displays List of Links ') ),
			array( 'width' => 400,
				   'height' => 350 )           
		);
 }     
        
 private function parse_instance_args( $instance ) {
		$instance = wp_parse_args( (array)$instance,
			array(
				'title' => '', 
				
    'linkIndex' => 0,       
			)
		);
		return $instance;
	}    
        
 public function widget( $args, $instance ) {

		$instance = $this->parse_instance_args( $instance ); 
  
  echo $args['before_widget'];
?>  
	<div class="toit-widget-links">
     <h3 class="toit-widget-links-title"><?php echo $instance['before_title']. $instance['title']. $instance['after_title']; ?> </h3>
      <ul class="toit-widget-links-ul">
<?php 
        for($i=1; $i < $instance['linkIndex']; $i++){
            if(!empty($instance['linkheading_'.$i]) && !empty($instance['link_'.$i]) ){
?>
                <li><a href="<?php echo $instance['link_'.$i] ; ?>" class="<?php echo $instance['linkclass_'.$i] ; ?>" <?php  if($instance['window_'.$i]=='new-window'){ ?> target="_blank" <?php } ?>><?php echo $instance['linkheading_'.$i] ; ?></a></li> 
<?php
            }else if(!empty($instance['linkheading_'.$i]) && empty($instance['link_'.$i]) ){ ?>
                <li class="<?php echo $instance['linkclass_'.$i] ; ?>"><?php echo $instance['linkheading_'.$i] ; ?></li> 
<?php       }
        }   
?>
      </ul>
	</div>

<?php  echo $args['after_widget'];
 }
 public function update( $new_instance, $old_instance ) {
  $instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['linkIndex'] = strip_tags( $new_instance['linkIndex'] );
  
  for($i=1; $i < $instance['linkIndex']; $i++){
    if(isset($new_instance['linkheading_'.$i]) && isset($new_instance['link_'.$i]) ){
        $instance['linkheading_'.$i] = strip_tags( $new_instance['linkheading_'.$i] );
        $instance['link_'.$i] = $new_instance['link_'.$i];
        $instance['window_'.$i] = $new_instance['window_'.$i];
        $instance['linkclass_'.$i] = $new_instance['linkclass_'.$i];
    }else{
     unset($instance['linkheading_'.$i]);
     unset($instance['link_'.$i]);
     unset($instance['window_'.$i]);
     unset($instance['linkclass_'.$i]);
    }
  }

  return $instance;
 } 
 public function form( $instance ) {

		$instance = $this->parse_instance_args( $instance );
		$title = $instance['title'];
?>
 <div class="toit-widget-links-form">
  <div class="toit-widget-links-field" id="toit-widget-links">
  <div class="toit-widget-links-field">
   <label for="<?php print $this->get_field_id( 'title' ); ?>"><?php print __( 'Title: ' ); ?>:</label>
   <input class="widefat" id="<?php print $this->get_field_id( 'title' ); ?>"
      name="<?php print $this->get_field_name( 'title' ); ?>" value="<?php print $title; ?>" type="text">
  </div>
<?php 
   for($i=1; $i < $instance['linkIndex']; $i++){
     if(isset($instance['linkheading_'.$i]) && isset($instance['link_'.$i]) && !empty($instance['linkheading_'.$i])  ){
?>
  <ul class="toit-widget-links-link" id="link-<?php echo $i; ?>">
   <li class="toit-widget-links-field">
    <label for="<?php print $this->get_field_id( 'linkheading_'.$i ); ?>"><?php print __( 'Enter Link Title: ' ); ?>:</label>
    <input id="<?php print $this->get_field_id( 'linkheading_'.$i ); ?>"
       name="<?php print $this->get_field_name( 'linkheading_'.$i ); ?>" value="<?php print $instance['linkheading_'.$i]; ?>" type="text">
   </li>            
   <li class="toit-widget-links-field">
    <label for="<?php print $this->get_field_id( 'link_'.$i ); ?>"><?php print __( 'Enter Link URL: ' ); ?>:</label>
    <input id="<?php print $this->get_field_id( 'link_'.$i ); ?>"
       name="<?php print $this->get_field_name( 'link_'.$i ); ?>" value="<?php print $instance['link_'.$i]; ?>" type="text">
   </li>
   <li class="toit-widget-links-field">
    <label for="<?php print $this->get_field_id( 'linkclass_'.$i ); ?>"><?php print __( 'Enter Link Class: ' ); ?>:</label>
    <input id="<?php print $this->get_field_id( 'linkclass_'.$i ); ?>"
       name="<?php print $this->get_field_name( 'linkclass_'.$i ); ?>" value="<?php print $instance['linkclass_'.$i]; ?>" type="text">
   </li>
   <li class="toit-widget-links-field">
    <label for="<?php print $this->get_field_id( 'window_'.$i ); ?>"><?php print __( 'Window Option:' ); ?></label>
    <select id="<?php print $this->get_field_id( 'window_'.$i  ); ?>" name="<?php print $this->get_field_name( 'window_'.$i  ); ?>">
        <option value="new-window" <?php if($instance['window_'.$i ] == 'new-window') echo 'selected="selected"'; ?>><?php print __( 'New Window' ); ?></option>
        <option value="same-window" <?php if($instance['window_'.$i ] == 'same-window') echo 'selected="selected"'; ?>><?php print __( 'Same Window' ); ?></option>
    </select>
   </li>
   <li><a href="javascript:" onclick="removelink(<?php echo $i; ?>)"><?php echo __('Remove Link'); ?> </a></li>
  </ul> 
<?php
            }
        }
?>
    <input type='hidden' id="<?php print $this->get_field_id( 'linkIndex' ); ?>" name="<?php print $this->get_field_name( 'linkIndex' ); ?>" value="<?php echo $i; ?>" />
   </div>
   <a href="javascript:" onclick="addnewlink(this)"><?php echo __('Add New Link'); ?> </a>  
    
   <div class="link_template" style="display:none;">
    <ul class="toit-widget-links-link">
     <li class="toit-widget-links-field">
      <label for="<?php echo $this->get_field_id( 'linkheading_#' ); ?>"><?php echo __( 'Enter Link Title: ' ); ?>:</label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'linkheading_#' ); ?>" name="<?php echo $this->get_field_name( 'linkheading_#' ); ?>" value="" type="text">
     </li>
     <li class="toit-widget-links-field">
      <label for="<?php echo $this->get_field_id( 'link_#' ); ?>"><?php echo __( 'Enter Link URL: ' ); ?>:</label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'link_#' ); ?>" name="<?php echo $this->get_field_name( 'link_#' ); ?>" value="" type="text">
     </li>
     <li class="toit-widget-links-field">
      <label for="<?php echo $this->get_field_id( 'linkclass_#' ); ?>"><?php echo __( 'Enter Link Class: ' ); ?>:</label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'linkclass_#' ); ?>" name="<?php echo $this->get_field_name( 'linkclass_#' ); ?>" value="" type="text">
     </li>
     <li class="toit-widget-links-field">
      <label for="<?php echo $this->get_field_id( 'window_#' ); ?>"><?php echo __( 'Window Option: ' ); ?>:</label>
      <select id="<?php echo $this->get_field_id( 'window_#' ); ?>" name="<?php echo $this->get_field_name( 'window_#' ); ?>">
       <option value="same-window"><?php echo __( 'Same Window' ); ?> </option>
       <option value="new-window"><?php echo __( 'New Window' ); ?> </option>
      </select>
     </li>
    </ul>
    <input type='hidden' name="linkIndexId" value="<?php print $this->get_field_id( 'linkIndex' ); ?>" />
    <input type='hidden' name="linkIndexName" value="<?php print $this->get_field_name( 'linkIndex' ); ?>" />

   </div>
 </div>
<script type="text/javascript">

function addnewlink(obj){

  var html = jQuery(obj).siblings('.link_template').html();
  
  var linkIndexId=jQuery(obj).siblings('.link_template').find('input[name="linkIndexId"]').val();
  var linkIndexName=jQuery(obj).siblings('.link_template').find('input[name="linkIndexName"]').val();
  
  var linkIndex =  jQuery("#"+linkIndexId).val();

  var newhtml = html.replace(/_#/g, "_"+linkIndex);
  linkIndex++;    
  jQuery("#"+linkIndexId).val(linkIndex);
  jQuery(obj).siblings('.toit-widget-links-field').append(newhtml);
    
}
function removelink(id){
  jQuery("#link-"+id).remove();
}

</script>
<?php
    } 
}

function theme_linkswidget_init(){
     register_widget( 'toitlinkswidget' );
}
add_action( 'widgets_init', 'theme_linkswidget_init');        
        
