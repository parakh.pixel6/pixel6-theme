<?php 

class toittwitterwidget extends WP_Widget {

public function __construct() {
		// widget actual processes
		parent::__construct(
			'theme-twitter-widget',
			__( 'Toit Twitter  Widget'),
			array( 'description' => __( 'Displays Recent/Random Posts ') ),
			array( 'width' => 400,
				  'height' => 350 )
		);

		if ( is_active_widget( false, false, $this->id_base ) ) {

			add_action( 'wp_head', array( &$this, 'enqueue_styles' ) );
		}
	}
    
	function parse_instance_args( $instance ) {
		$instance = wp_parse_args( (array)$instance,
			array(
				'title' => '',
				'username' => '',
                'consumer_key'=>'',
                'consumer_secret'=>'',
                'access_token'=>'',
                'access_token_secret'=>'',
    
			)
		);

		return $instance;
	}
    function enqueue_styles() {

		wp_enqueue_style( 'toit-widget-css', get_bloginfo('stylesheet_directory'). '/library/css/widget.css');
	}
    
 function getTweets( $widget_id, $instance){
  
 	$timeline = $cb->statuses_userTimeline( 'screen_name=' . $instance['username']. '&count=' . $instance['limit'] . '&exclude_replies=true' );
 	return $timeline;
 }  
    
  function widget($args, $instance){
			
        global $wp_query;
		extract( $args );
		$instance = $this->parse_instance_args( $instance );
		$instance['title'] = apply_filters( 'widget_title', $instance['title'] );	
        $username = $instance['username'];
		$intro = $instance['intro'];    
        echo $args['before_widget'];   	
		echo $before_widget;	
		echo $before_title.$title.$after_title;
	   
		echo $after_widget;
			
	}  
    
 
    
    function update($new_instance, $old_instance){
		
		$instance = $old_instance;		
		$instance['title'] =strip_tags($new_instance['title']);
		$instance['username'] = strip_tags($new_instance['username']);
		$instance['intro'] = strip_tags($new_instance['intro']);
        $instance['consumer_key'] = strip_tags($new_instance['consumer_key']);
        $instance['consumer_secret'] = strip_tags($new_instance['consumer_secret']);
        $instance['access_token'] = strip_tags($new_instance['access_token']);
        $instance['access_token_secret'] = strip_tags($new_instance['access_token_secret']);
		return $instance;
			
	}
    
    function form($instance){		
		$instance = $this->parse_instance_args( $instance );
		$title = strip_tags( $instance['title'] );	
     
		?>
<div class="toit-widget-form">
  <ul class="toit-widget-fromul">
        <li>
            <label for="<?php print $this->get_field_id( 'title' ); ?>"><?php print __( 'Title: ' ); ?>:</label>
            <input class="widefat" id="<?php print $this->get_field_id( 'title' );?>"
               name="<?php print $this->get_field_name( 'title' ); ?>" value="<?php print $title; ?>" type="text">
        </li>
        <li>
        <label for="<?php print $this->get_field_id( 'username' ); ?>"><?php print __( 'Enter Username: ' ); ?></label>
        <input type="text"  value="<?php print $instance['username']; ?>"
               name="<?php print $this->get_field_name( 'username' ); ?>"
               id="<?php print $this->get_field_id( 'username' ); ?>">
        </li>
         <li>
        <label for="<?php print $this->get_field_id( 'consumer_key' ); ?>"><?php print __( 'Enter Consumer key: ' ); ?></label>
        <input type="text"  value="<?php print $instance['consumer_key']; ?>"
               name="<?php print $this->get_field_name( 'consumer_key' ); ?>"
               id="<?php print $this->get_field_id( 'consumer_key' ); ?>">
        </li>
        <li>
        <label for="<?php print $this->get_field_id( 'consumer_secret' ); ?>"><?php print __( 'Enter Consumer Secret: ' ); ?></label>
        <input type="text"  value="<?php print $instance['consumer_secret']; ?>"
               name="<?php print $this->get_field_name( 'consumer_secret' ); ?>"
               id="<?php print $this->get_field_id( 'consumer_secret' ); ?>">
        </li> 
         <li>
        <label for="<?php print $this->get_field_id( 'access_token' ); ?>"><?php print __( 'Enter Access Token: ' ); ?></label>
        <input type="text"  value="<?php print $instance['access_token']; ?>"
               name="<?php print $this->get_field_name( 'access_token' ); ?>"
               id="<?php print $this->get_field_id( 'access_token' ); ?>">
        </li>
         <li>
        <label for="<?php print $this->get_field_id( 'access_token_secret' ); ?>"><?php print __( 'Enter Access Token Secret: ' ); ?></label>
        <input type="text"  value="<?php print $instance['access_token_secret']; ?>"
               name="<?php print $this->get_field_name( 'access_token_secret' ); ?>"
               id="<?php print $this->get_field_id( 'access_token_secret' ); ?>">
        </li>     
   </ul>
</div>
<?php 	
	}
}

function theme_twitter_widget_init(){
     register_widget( 'toittwitterwidget' );
}
add_action( 'widgets_init', 'theme_twitter_widget_init');