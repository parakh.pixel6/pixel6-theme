<?php

  class toitflickerwidget extends WP_Widget 
  {
   private $flickr_api_key = "d348e6e1216a46f2a4c9e28f93d75a48";

   public function __construct() 
   {
		   // widget actual processes
		   parent::__construct(
			  'theme-flicker-widget',
			  __( 'Toit Flicker Widget'),
			  array( 'description' => __('Flickr Images') )
		      );
	  }
   function parse_instance_args( $instance ) 
   {
		    $instance = wp_parse_args( (array)$instance,
			  array(
				   'widgetname' => '',
       'username'=>'',
       'view'=>'',
				   'number'=> 3,
				   'postytpe' => 'post',
				   'displaytype' => 'listing',
       'show_description'=>'',
      )
		    );
      return $instance;
	  }

    public function widget( $args, $instance ) 
    {
       extract( $args );
		   $instance = $this->parse_instance_args( $instance );
		   $instance['widgetname'] = apply_filters( 'widget_title', $instance['widgetname'] );		
       $user_id = $instance['user_id'];
       $number = $instance[ 'number' ];
       $description = $instance['show_description'];
       $view = $instance[ 'view' ];
       echo $args['before_widget'];
       if (!ereg("http://api.flickr.com/services/feeds", $username))
				     $url = "http://api.flickr.com/services/feeds/photos_public.gne?id=".urlencode($user_id)."&format=php_serial&lang=en-us";
		     else
				    $url = $username."&format=php_serial";
                $feed = @file_get_contents($url);
                $photos = (unserialize($feed));
?>
        <div class="toit-widget-flicker">
         <h3 class="toit-widget-flicker-title"><?php if($instance['widgetname']) echo $instance['widgetname'] ; ?></h3>
         <div class="toit-widget-flicker-wrapper clearfix">
          <?php if($photos)
          {
           foreach($photos["items"] as $key => $value)
          {
            if ($key >= $number) break;
            $photo_title = $value["title"];
            $url=$value["url"];
            $photo_description = str_replace("\n", "", strip_tags($value["description"]));
            $photo_url = str_replace("_b.jpg", "$view.jpg", $value["photo_url"]);
?>
           <div class="toit-widget-flicker-field" >
           
             <a class="toit-widget-flicker-photos" href="<?php echo $url ;?>" target="_blank" ><img src="<?php echo $photo_url ;?>" alt="<?php echo esc_attr($photo_title) ; ?>"/></a>
             <?php if($instance['title']){ ?><a class="toit-widget-flicker-photos-title" href="<?php echo $url ;?>" target="_blank"><?php echo $photo_title ; ?></a><?php } ?> 
             <?php if($instance['show_description'])
             { ?>
               <p class="toit-widget-flicker-photos-info"><?php if(strlen($photo_description) < 100)
                {
                  echo $photo_description ; 
                }else{
                       echo substr($photo_description, 0 , 100) ."...";
                     }?>
               </p> 
       <?php } ?> 
           </div>
<?php	} } ?>
          </div>
         </div>
        <?php echo $args['after_widget'];
     }
         public function update( $new_instance, $old_instance )
            {
             $instance = $old_instance;
             $instance['widgetname']= strip_tags( $new_instance['widgetname'] );
             $instance['title'] = strip_tags( $new_instance['title'] );
             $instance['username'] = strip_tags($new_instance['username']);
             $instance['view'] = $new_instance['view'];
             $instance['number'] = is_int($new_instance['number']) ? $new_instance['number'] : 3;
             $instance['show_description'] = $new_instance['show_description'];
              if (!empty($instance["username"]) && $instance["username"] != $old_instance["username"]) {
              if (!ereg("http://api.flickr.com/services/feeds", $instance["username"])) // Not a feed
			            {
                $str = file_get_contents("http://api.flickr.com/services/rest/?method=flickr.people.findByUsername&api_key=".$this->flickr_api_key."&username=".urlencode($instance["username"])."&format=rest");
                ereg("<rsp stat=\\\"([A-Za-z]+)\\\"", $str, $regs); $findByUsername["stat"] = $regs[1];
                if ($findByUsername["stat"] == "ok")
                {
                 ereg("<username>(.+)</username>", $str, $regs);
                 $findByUsername["username"] = $regs[1];
                 
                 ereg("<user id=\\\"(.+)\\\" nsid=\\\"(.+)\\\">", $str, $regs);
                 $findByUsername["user"]["id"] = $regs[1];
                 $findByUsername["user"]["nsid"] = $regs[2];
                 
                 $flickr_id = $findByUsername["user"]["nsid"];
                 $newoptions["error"] = "";
                }
                else
                {
                 $flickr_id = "";
                 $newoptions["username"] = ""; // reset
                 
                 ereg("<err code=\\\"(.+)\\\" msg=\\\"(.+)\\\"", $str, $regs);
                 $findByUsername["message"] = $regs[2] . "(" . $regs[1] . ")";
                 
                 $newoptions["error"] = "Flickr API call failed! (findByUsername returned: ".$findByUsername["message"].")";
                }
                $instance["user_id"] = $flickr_id;
               } else {
                $newoptions["error"] = "";
               }
              } else { 
               $instance["user_id"] = $old_instance["user_id"];
              }
              return $instance;
            }
            public function form( $instance ) 
            {
             $instance = $this->parse_instance_args( $instance );
             $widgetname = $instance['widgetname'] ;
             $title = $instance['title'] ;
             $username = $instance['username'];
             $view = $instance['view'];
             $number = $instance['number'];
             $description = $instance['show_description'];

?>
            <div class="toit-widget-form">
                <ul class="toit-widget-fromul">
	                 <li>
                    <label for="<?php print $this->get_field_id( 'widgetname' ); ?>"><?php print __( 'Title: ' ); ?>:</label>
                    <input id="<?php print $this->get_field_id( 'widgetname' ); ?>"
                       name="<?php print $this->get_field_name( 'widgetname' ); ?>" value="<?php print $instance['widgetname']; ?>" type="text">
                   </li>
                   <li>
		                   <label for="<?php print $this->get_field_id( 'username' ); ?>"><?php print __( 'Flickr RSS URL: ' ); ?>:</label>
		                   <input id="<?php print $this->get_field_id( 'username' ); ?>"
		                     name="<?php print $this->get_field_name( 'username' ); ?>" value="<?php print $instance['username']; ?>" type="text">
                   </li>
                   <li>
                       <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e("How many items?"); ?> </label>
                       <input id="<?php print $this->get_field_id( 'number' ); ?>"
                       name="<?php print $this->get_field_name( 'number' ); ?>" value="<?php print $instance['number']; ?>" type="text">
                 
                   </li>
                   <li>
                     <label for="<?php print $this->get_field_id( 'view' ); ?>"><?php print __( 'Thumbnail, square or medium?: ' ); ?></label>
                       <select class="widefat" id="<?php echo $this->get_field_id( 'view' ); ?>" name="<?php echo $this->get_field_name( 'view' ); ?>">
                         <option value="_t" <?=($view=="_t" ? "selected=\"selected\"" : "");?>>Thumbnail</option>
                         <option value="_s" <?=($view=="_s" ? "selected=\"selected\"" : "");?>>Square</option>
                         <option value="_m" <?=($view=="_m" ? "selected=\"selected\"" : "");?>>Medium</option>
                         <option value="" <?=($view=="" ? "selected=\"selected\"" : "");?>>Large</option>
                      </select>
                   </li>
                    <li>
                       <label for="<?php print $this->get_field_id( 'title' ); ?>"><?php print __('Show Photo Title:'); ?></label>
                       <input type="checkbox"  value="1"
                        <?php if($instance['title']) echo 'checked="checked"';?>
                          name="<?php print $this->get_field_name('title'); ?>"
                          id="<?php print $this->get_field_id( 'title' );?> ">
                    </li>
                    <li>
                      <label for="<?php print $this->get_field_id( 'show_description' ); ?>"><?php print __('Show Photo Discription:'); ?></label>
                      <input type="checkbox"  value="1"
                        <?php if($instance['show_description']) echo 'checked="checked"';?>
                           name="<?php print $this->get_field_name('show_description'); ?>"
                           id="<?php print $this->get_field_id( 'show_description' );?> ">
                     </li>
                  </ul>
                </div>
<?php 
           }
  }
         function theme_flicker_init()
         {
            register_widget( 'toitflickerwidget' );
          }
          add_action( 'widgets_init', 'theme_flicker_init');
