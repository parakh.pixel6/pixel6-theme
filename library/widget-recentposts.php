<?php
class toitrecentpostwidget extends WP_Widget {

	public function __construct() {
		// widget actual processes
		parent::__construct(
			'theme-recent-post-widget',
			__( 'Toit Recent Post Widget'),
			array( 'description' => __( 'Displays Recent/Random Posts ') ),
			array( 'width' => 400,
				  'height' => 350 )
		);

	}
	
	function parse_instance_args( $instance ) {
		$instance = wp_parse_args( (array)$instance,
			array(
				'title' => '',
				'number' => 2,
				'postytpe' => 'post',
				'displaytype' => 'listing',
				'slider_duration' => 1500,
			)
		);

		return $instance;
	}

	public function widget( $args, $instance ) {
		// outputs the content of the widget
		global $wp_query;
		extract( $args );
		$instance = $this->parse_instance_args( $instance );
		
  $instance['title'] = apply_filters( 'widget_title', $instance['title'] );		
                 echo $args['before_widget'];
   $query=  array('posts_per_page' => $instance['number'],
						           'post_type' => $instance['postytpe'] );
       if($instance['order'] == 'random') {
			          $query['orderby'] ='rand';
		         }else{
			       $query['order'] ='DESC';
			       $query['orderby'] ='post_date';
		           }
		          $posts = get_posts( $query );
		          $no_of_posts = count($posts);
?>
		
        <div class="toit-widget-recentpost <?php if($instance['displaytype'] == 'slider') echo 'widget-slider-wrapper'; ?> toit-widget-<?php print $instance['theme'] ?>">
		    <h3 class="toit-widget-recentpost-title"><?php echo   $instance['title'] ; ?></h3>
		    <a class="widget-viewall" href="<?php echo $instance['viewall'] ; ?>" ><?php echo $instance['viewalltext'] ; ?></a>
		   	<div class="clear"></div>
		    <ul class="toit-widget-recentpost-ul <?php if($instance['displaytype'] == 'slider') echo 'toit-widget-slider'; ?>">
<?php
			 foreach($posts as $index=>$post)
			 {		 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
				 if($instance['displaytype'] == 'slider' && $index > 0)  $hide="toit-hide";
				 else $hide="";
?>
			<li id="toit-widget-post-<?php echo $index; ?>" class="toit-post  <?php if($instance['displaytype'] == 'slider') echo 'toit-widget-post-slider'; ?> <?php echo $hide; ?>" data-index="<?php echo $index; ?>">
				<?php if($instance['show_thumb'] && $thumb!='') { ?>
				<div class="toit-left">
					<a href="<?php echo get_permalink($post->ID);?>"><img height="56" width="56" alt="twitter3" class="wp-post-image" src="<?php echo $thumb[0];  ?>"  /></a>
				</div>
				<?php } ?>	
				<div class="toit-widget-recentpost-info">
					<div class="toit-widget-recentpost-title">
					<a class="toit-widget-recentpost-link" href="<?php echo get_permalink($post->ID);?>"> <?php echo $post->post_title;?> </a>
				<?php if($instance['show_author']) { ?>
					 <br> by <a class="toit-widget-recentpost-author" href="<?php echo get_author_posts_url($post->post_author);?>"><?php echo the_author_meta( 'display_name', $post->post_author ); ?> </a>
				<?php } ?>		
					</div>
				</div>
				<div class="clear"></div>
			</li>
<?php		
			 }
?>			 
		    </ul>	
	    </div>

<?php if($instance['displaytype'] == 'slider') { ?>
<script>
 jQuery(document).ready(function($){
  	setInterval(toit_rotate_slides, 2000);
	function toit_rotate_slides(){
		var selected = '';
		jQuery(".toit-widget-recentpost-ul li").each(function(){
			if(!jQuery(this).hasClass('toit-hide')){
				selected = jQuery(this);
			}
		});
		var index = selected.attr('data-index');
		if(index >= <?php echo $no_of_posts-1; ?>) index = 0;
		else index++;
		
		selected.animate({opacity: 0}, <?php echo $instance['slider_duration']; ?>, function(){jQuery(this).addClass('toit-hide');});
		jQuery('#toit-widget-post-'+index).animate({opacity: 1}, <?php echo $instance['slider_duration']; ?>, function(){jQuery(this).removeClass('toit-hide'); jQuery(this).show();});

	}
 });
</script>
<?php } ?>
		  
<?php		  
		  echo $args['after_widget'];
	}

 

	public function update( $new_instance, $old_instance ) {
	
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = (is_numeric($new_instance['number'])) ? $new_instance['number'] : 3;
		$instance['order'] = (!empty($new_instance['order'])) ? $new_instance['order'] : 'recent';
		$instance['postytpe'] = (!empty($new_instance['postytpe'])) ? $new_instance['postytpe'] : 'post';
		$instance['show_thumb'] = $new_instance['show_thumb'];
		$instance['show_author'] = $new_instance['show_author'];
		$instance['displaytype'] =  $new_instance['displaytype'] ;	
		$instance['slider_duration'] =  (is_numeric($new_instance['slider_duration'])) ? $new_instance['slider_duration'] : 1500;
		$instance['viewall'] = strip_tags( $new_instance['viewall'] );	
		$instance['viewalltext'] = strip_tags( $new_instance['viewalltext'] );	
		return $instance;
	}
	
	public function form( $instance ) {

		$plugin_dir = plugin_dir_url( __FILE__ );
		$instance = $this->parse_instance_args( $instance );
		$title = strip_tags( $instance['title'] );
		$viewall = strip_tags( $instance['viewall'] );
		$viewalltext = strip_tags( $instance['viewalltext'] );
?>
<div class="toit-widget-form">
  <ul class="toit-widget-fromul">
	<li>
		<label for="<?php print $this->get_field_id( 'title' ); ?>"><?php print __( 'Title: ' ); ?>:</label>
		<input class="widefat" id="<?php print $this->get_field_id( 'title' ); ?>"
		   name="<?php print $this->get_field_name( 'title' ); ?>" value="<?php print $title; ?>" type="text">
    </li>
	<li>
	<label
		for="<?php print $this->get_field_id( 'number' ); ?>"><?php print __( 'Number of posts: ' ); ?></label>
	<input type="text" size="3" value="<?php print $instance['number'];?>"
		    name="<?php print $this->get_field_name( 'number' ); ?>"
		    id="<?php print $this->get_field_id( 'number' ); ?>">
    </li>
	<li>
	<label for="<?php print $this->get_field_id( 'order' ); ?>"><?php print __( 'Order: ' ); ?></label>
	<select id="<?php print $this->get_field_id( 'order' ); ?>"  name="<?php print $this->get_field_name( 'order' ); ?>">
		 <option value="recent" <?php if($instance['order'] == 'recent') echo 'selected="selected"'; ?>><?php print __( 'Latest' ); ?></option>
		 <option value="random" <?php if($instance['order'] == 'random') echo 'selected="selected"'; ?>><?php print __( 'Random' ); ?></option>
	</select>
	 </li>
	<li>
	<label for="<?php print $this->get_field_id( 'postytpe' ); ?>"><?php print __( 'Post Type: ' ); ?></label>
	<select id="<?php print $this->get_field_id( 'postytpe' ); ?>"  name="<?php print $this->get_field_name( 'postytpe' ); ?>">
		 <option <?php if($instance['postytpe'] == 'post') echo 'selected="selected"'; ?> value="post"><?php print __( 'Post' ); ?></option>
		<?php $post_types = get_post_types( array('_builtin' => false), 'objects' );
			foreach($post_types as $post_type){ ?>
		 <option <?php if($instance['postytpe'] == $post_type->name) echo 'selected="selected"'; ?>  value="<?php echo $post_type->name; ?>"><?php echo $post_type->label; ?></option>
		 <?php } ?>
	</select>
	</li>
	<li>
		<label
			for="<?php print $this->get_field_id( 'show_thumb' ); ?>"><?php print __( 'Show Post Thumb: ' ); ?></label>
		<input type="checkbox" size="3" value="1"
				<?php if($instance['show_thumb']) echo 'checked="checked"';?>
			    name="<?php print $this->get_field_name( 'show_thumb' ); ?>"
			    id="<?php print $this->get_field_id( 'show_thumb' );?> ">
    </li>
	<li>
		<label
			for="<?php print $this->get_field_id( 'show_author' ); ?>"><?php print __( 'Show Author Link: ' ); ?></label>
		<input type="checkbox" size="3" value="1"
				<?php if($instance['show_author']) echo 'checked="checked"'; ?>
			   name="<?php print $this->get_field_name( 'show_author' ); ?>"
			   id="<?php print $this->get_field_id( 'show_author' ); ?>">
    </li>
	<li>
		<label for="<?php print $this->get_field_id( 'displaytype' ); ?>"><?php print __( 'Display Type: ' ); ?></label>
		<select id="<?php print $this->get_field_id( 'displaytype' ); ?>"  name="<?php print $this->get_field_name( 'displaytype' ); ?>">
			 <option value="listing" <?php if($instance['displaytype'] == 'listing') echo 'selected="selected"'; ?>><?php print __( 'Listing' ); ?></option>
			 <option value="slider" <?php if($instance['displaytype'] == 'slider') echo 'selected="selected"'; ?>><?php print __( 'Slider' ); ?></option>
		</select>
	</li>
	<li>
	<label
		for="<?php print $this->get_field_id( 'slide_duration' ); ?>"><?php print __( 'Slider Slide Duration: ' ); ?></label>
	<input type="text" size="3" value="<?php print $instance['slide_duration'];?>"
		   name="<?php print $this->get_field_name( 'slide_duration' ); ?>"
		   id="<?php print $this->get_field_id( 'slide_duration' ); ?>">
    </li>
	<li>
	 <label for="<?php print $this->get_field_id( 'viewall' ); ?>"><?php print __( 'View All Link: ' ); ?>:</label>
		<input class="widefat" id="<?php print $this->get_field_id( 'viewall' ); ?>"
		   name="<?php print $this->get_field_name( 'viewall' ); ?>" value="<?php print $viewall; ?>" type="text">
	</li>
	<li>
	 <label for="<?php print $this->get_field_id( 'viewalltext' ); ?>"><?php print __( 'View All Text: ' ); ?>:</label>
		<input class="widefat" id="<?php print $this->get_field_id( 'viewalltext' ); ?>"
		   name="<?php print $this->get_field_name( 'viewalltext' ); ?>" value="<?php print $viewalltext; ?>" type="text">
	</li>
   </ul>
</div>
<?php 	
	}
}

function theme_widgets_init(){
     register_widget( 'toitrecentpostwidget' );
}
add_action( 'widgets_init', 'theme_widgets_init');
