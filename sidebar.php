<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package pixel
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<div class="coloumn_25">
		<div class="px_sidebar_details">
			<div class="px_sidebar_heading">
				<h3>TOPICS</h3>
				<span>What we talk about</span>
			</div>
			<div class="px_aside_desc">
				<ul>
				<?php
					$args = array( 'numberposts' => '5' );
					$recent_posts = wp_get_recent_posts( $args );
					foreach( $recent_posts as $recent ){
						echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
					}
				?>
				</ul>
			</div><!-- #secondary -->

		</div>
		<div class="px_sidebar_details">
			<div class="px_sidebar_heading">	
				<h3>ARCHIVE</h3>
				<span>past thoughts</span>
			</div>				
			<div class="px_aside_desc">
				<ul>
					<?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 5 ) ); ?>
				</ul>
			</div><!-- #secondary -->

		</div>
	</div>
	<div class="coloumn_25 last_coloumn_25">
		<div class="project_details">
			<div class="px_sidebar_heading">
				<h3>Tags</h3>
				<span>favourite topics</span>
			</div>
			<div class="px_aside_desc">
				<?php $tags = get_tags();
				$html = '<ul>';
				foreach ( $tags as $tag ) {
					$tag_link = get_tag_link( $tag->term_id );

					$html .= "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
					$html .= "{$tag->name}</a></li>";
				}
				$html .= '</ul>';
				echo $html;
				?>
			</div>
		</div>
	</div>
</div>