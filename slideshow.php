<?php $firstPosts = get_posts(array( 'posts_per_page' => 5,'post_type' => 'portfolio' , 'portfolio_category'  => 'slideshow' ));  ?>

<div class="slides_wrappper clearfix">
	<div class="project_details_img"><img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/project_mberry.png" alt="slide1" /></div>
	<div class="px_slideshow px_bxslider clearfix">

		<?php  foreach($firstPosts as $firstPost){
		$post_title = get_post_meta($post->ID,'theme_post_subtitle_value', true); 
		$visitProject = get_post_meta($firstPost->ID,'portfolioLink_value', true);
		?>
		<div class="slides slide_1 slideanimate">
			<div class="px_slideshow_left">
				<div class="px_post_heading">
					<h3 class="slide_title"><?php echo get_the_title($firstPost->ID);?></h3>
					<?php if(!empty($post_title)){ ?>
						<span class="slide_subtitle"><?php echo $post_title?></span>		
					<?php }?>					
				</div>
				<div class="px_post_content">
					<div class="post_content_slider">
						<?php echo apply_filters('the_content', $firstPost->post_content); ?>
					</div>
				</div>
				<div class="px_post_tags">
					<?php  echo get_the_term_list( $firstPost->ID, 'post_tag', '<ul class="skills-list"><li>','</li><li>','</li></ul>');?>
				</div>
<!-- 				<a href="<?php //echo $visitProject;?>" class="white_button view_button clearfix" target="_blank">Visit project <span>&raquo;</span></a> -->
				
			</div>
			<?php if(has_post_thumbnail($firstPost->ID)){?>
			<div class="px_slideshow_right">
				<?php echo get_the_post_thumbnail($firstPost->ID); ?>
			</div>
			
			<!-- <a href="<?php //echo get_permalink($firstPost->ID);?>" class="white_button view_button clearfix">View project <span>&raquo;</span></a> -->
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</div>
<!-- $tag_list = $tags = get_the_term_list( $post->ID, 'custom-tag', '<ul class="snpt-tags"><li>','</li><li>','</li></ul>');
print $tag_list; -->