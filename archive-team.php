<?php
/**
* @package WordPress
* @subpackage moto
* Template Name: Team archives
*/
get_header(); ?>
<section class="clearfix px_container profile">
	<div class="px_container_top content_wrapper_width clearfix">
		<div class="clearfix">
			<div class="px_1coloumn coloumn_first">
				<div class="px_project_heading">
					<h2>THE SERVICES </h2>
					<span>what we do</span>
				</div>
				<div class="px_project_desc clearfix">
					<ul class="clearfix">
						<li>UX Engineering</li>
						<li>Front End Development</li>
						<li>Full Stack Development</li>
						<li>eCommerce</li>
						<li>Mobile Apps</li>
						<li>Websites &amp; Web Applications</li>
						<li>Wordpress, ModX, ProcessWire, EE</li>
						<li>Magento, Shopify, Zencart</li>
						<li>x-Browser &amp; x-OS compatibility</li>
						<li>Usability and Accessibility</li>
						<li>SEO and SEM</li>					
					</ul>				
				</div>
				<div class="testimonial_sec clearfix">
			
					<?php 

					$args = array( 'post_type' => 'acme_quote', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC');
					$posts_array = get_posts( $args );
					// print_r($posts_array);
					foreach ($posts_array as $key => $value) {
						$quote = $posts_array[$key];
						$user = $quote->post_author;

					?>
						<div class="px_3coloumn">
							<blockquote class="px_service_quote px_whitebg_quote">
								<div class="px_author_sec">
									<div class="quote-author-img">
									<?php if(has_post_thumbnail($quote->ID)){?>
										<?php echo get_the_post_thumbnail($quote->ID); ?>
									<?php } else{?>
										<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/user-placeholder.png">
										<?php }?>
									</div>
									<div class="auther_details">
										<?php echo $quote->post_title; ?>
										<p><small><?php echo $quote->post_content; ?></small></p>
									</div>
								</div>
							</blockquote>
						</div>
					<?php } ?>

				</div>
			</div>
			<div class="px_2combinecoloumn coloumn_last">
				<div class="px_project_heading">
					<h2>THE TEAM</h2>
					<span>who we are</span>
				</div>
				<div class="px_project_desc clearfix">
					<div class="px_team_details clearfix">
						<div class="team">
							<?php $firstPosts = get_posts(array( 'posts_per_page' => 99,'post_type' => 'team', 'order' => 'ASC'));  ?>
							<?php foreach($firstPosts as $firstPost){ ?>
								<?php $singleitemclass = get_post_meta($firstPost->ID,'itemclass_value', true);?>	
						        <div class="team-member <?php if($singleitemclass) { echo $singleitemclass; } ?>">
						            <figure>
						            	
						                <?php echo get_the_post_thumbnail($firstPost->ID, 'medium');?>
						                <figcaption>
						                	
						                	<?php
											   $emp_name = get_the_title($firstPost->ID); // some IP address
											   $emp_array = preg_split ("/\s+/", $emp_name); 
											   
											?>
						                    <span class="member-name"><?php echo($emp_array[0]);//echo get_the_title($firstPost->ID);?></span>
						                    <span class="member-title"><?php echo get_post_field('post_content', $firstPost->ID);?></span>
						                </figcaption>
						            </figure>
						        </div>
					   		<?php } ?>
					    </div>
						
					</div>
					<?php // wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	 jQuery(document).ready(function(){

		var $grid = jQuery('.team').masonry({
			itemSelector: '.team-member',
			isAnimated: true,
			isFitWidth: true
		});
		setTimeout(function() {$grid},3000)
	});
	  
</script>
<?php get_footer(); ?>