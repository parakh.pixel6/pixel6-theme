<section class="clearfix px_container profile">

	<div class="px_container_top content_wrapper_width clearfix">

		<div class="px_1coloumn coloumn_first">

			<div class="px_project_heading">

				<h2>Our Capabilities </h2>

				<span>we are amazing at</span>

			</div>

			<div class="px_project_desc clearfix">

				<ul class="clearfix">

					<li>Strategy</li>

					<li>User Experience</li>

					<li>Web Development</li>

					<li>Mobile Apps</li>

					<li>Project Planning</li>

					<li>Project Management</li>

					<li>UI/UX Audit</li>

					<li>Design & Brand Strategy</li>

					<li>Digital Transformation</li>

					<li>Technical Support</li>					

				</ul>				

			</div>

			<div class="testimonial_sec clearfix">

				<?php 

				$args = array( 'post_type' => 'acme_quote', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC');

				$posts_array = get_posts( $args );

				foreach ($posts_array as $key => $value) {

					$quote = $posts_array[$key];

					$user = $quote->post_author;

				?>

					<div class="px_3coloumn">

						<blockquote class="px_service_quote px_whitebg_quote">

							<div class="px_author_sec">

								<div class="quote-author-img">

								<?php if(has_post_thumbnail($quote->ID)){?>

									<?php echo get_the_post_thumbnail($quote->ID); ?>

								<?php } else{?>

									<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/user-placeholder.png">

									<?php }?>

								</div>

								<div class="auther_details">

									<?php echo $quote->post_title; ?>

									<p><small><?php echo $quote->post_content; ?></small></p>

								</div>

							</div>

						</blockquote>

					</div>

				<?php } ?>



			</div>

		</div>

		<div class="px_2combinecoloumn coloumn_last">

			<div class="px_project_heading">

				<h2><?php the_title();?></h2>
				<span>who we are & what we do</span>

			</div>

			<div class="px_project_desc">
				<?php if (have_posts()) {
  while (have_posts()) {
			 	echo get_the_content();
}}
			 	?>
			</div> 

		</div>

	</div>

</section>

<script type="text/javascript">

	 jQuery(document).ready(function(){



	 	setTimeout(function(){

	 		jQuery('.team').imagesLoaded( { background: '.team-member' }, function() {

				jQuery('.team').masonry({

					itemSelector: '.team-member',

					isAnimated: true,

					isFitWidth: true

				});

			});

	 	}, 3500)

		

	});

</script>