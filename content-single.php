<?php
/**
 * @package pixel
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- <header class="entry-header">
		<div class="px_project_heading">
			<?php //the_title( '<h2 class="entry-title">', '</h2>' ); ?>
			<a href="<?php //echo home_url( '/' ); ?>" class="author_name"><span>by</span> pixel6 <span>on <?php //echo get_the_date();?></span></a>
		</div>

	</header>.entry-header -->

	<div class="entry-content px_service_quote px_whitebg_quote">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'pixel' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
