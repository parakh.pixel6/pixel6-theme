<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<section class="clearfix px_container banner-section">
			<div class="px_container_top content_wrapper_width clearfix">
				<div class="px_1coloumn coloumn_first">
					<div class="px_project_heading">
						<h2>Contact Pixel6</h2>
						<span>get in touch with us</span>
					</div>
					<div class="px_project_desc clearfix">
						<p>Please use the form on the right to get in touch with us. If you are looking for a quote or want to enquire about our services, please tell us about your project requirements and if you have any questions or just want to say HELLO, we will be more than happy to talk to you!</p>
						<h3>Location</h3>
						<p>
							V18, Balewadi High Street, <br>Baner, Pune, Maharashtra <br>India 411045
						</p>
<!-- 						<p>A3 Surashree, Nargis Dutt Road,<br> Model Colony, Pune <br> India 411016	<br> -->
							<!-- <a class="google-map-pin" href="https://www.google.com/maps/place/Pixel6+Web+Studio+Pvt.+Ltd./@18.5312684,73.840282,17z/data=!3m1!4b1!4m5!3m4!1s0x3bc2c07c427553b1:0xf926a2a068d70d09!8m2!3d18.5312684!4d73.8424707" target="_blank">Location on Google map</a> --> </p>
						<!-- <p>We are located in San Diego, California but work with  clients all over the world</p> -->
						<h3>Details</h3>
						<ul class="px_view_section px_contact_section">
							<li><span>E-mail us </span>  /  <a href="mailto:info@pixel6.co" id="px_contect_info"> info@pixel6.co</a>	</li>
							<li>Phone  /	<a href="tel:+918805060506">+91 88 05 06 05 06</a></li>
						</ul>
						<?php $firstPosts =get_posts(array( 'posts_per_page' => 1,'post_type' => 'testimonial' ));  ?>
						<?php foreach($firstPosts as $firstPost){ ?>
						<div class="px_3coloumn">
							<blockquote class="px_service_quote px_whitebg_quote">
								<div class="px_author_sec">
									<?php if(has_post_thumbnail($firstPost->ID)){?>
									<?php echo get_the_post_thumbnail($firstPost->ID, array(45, 45)); ?>
									<?php } ?>
									<div class="auther_details">
										<p><?php echo apply_filters('the_excerpt', $firstPost->post_content); ?><span class="author_name">&dash;<?php echo get_the_author($firstPost->ID);?></span></p>
									</div>
								</div>
							</blockquote>
						</div>
						<?php }?>
					</div>
				</div>
				<div class="px_2combinecoloumn coloumn_last">
				<!-- <span class="white_button px_reqest_btn">Request a quote or just say Hello!</span> -->
				<div class="coloum_btn_wrapper coloumn_last-contact ">
					<a href="#contact"  class="white_button" id="contact"> Contact</a>
				</div>
				<div class="coloum_btn_wrapper  coloumn_last-request ">
					<a href="#request" class="white_button" id="request"> Request a quote</a>
				</div>						
				<div class="px_service_quote px_whitebg_quote" >
					<div class="px_cont_form">
						<div class="px-form" id="form_contact">
							<?php echo do_shortcode( '[contact-form-7 id="236" title="contact Form"]' ); ?>
							<!-- local contact form-->	
							<?php //echo do_shortcode( '[contact-form-7 id="220" title="Contact form 1"]' ); ?>
								
						</div>
						<div class="px-form"  id="form_request">
							<?php //echo do_shortcode( '[thinkit-wp-contact-form 2]' ); ?> 
							<?php echo do_shortcode( '[contact-form-7 id="220" title="Request a quote"]' ); ?> 
							<!-- local request form-->													
							<?php //echo do_shortcode( '[contact-form-7 id="221" title="Contact form 1"]' ); ?>

						</div>
					</div>
				</div>
			</div>
		</section>
	</main><!-- #main -->
</div>
<div class="map-section">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.0830411412403!2d73.77346861420492!3d18.570294572499915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2b939c980f80f%3A0x10bfa44ac46e6781!2sPixel6%20Web%20Studio%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1665387373422!5m2!1sen!2sin" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	
<!-- 	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.082826027654!2d73.7732802154584!3d18.57030427249679!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf99986c4683%3A0x349d19b53f212ccd!2sV18%2C%20Balewadi%20High%20Street!5e0!3m2!1sen!2sin!4v1637923989531!5m2!1sen!2sin" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
<!-- 	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.9476341691197!2d73.84028201486457!3d18.531268387402825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c07c427553b1%3A0xf926a2a068d70d09!2sPixel6%20Web%20Studio%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1600067952619!5m2!1sen!2sin" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
	<!--<a class="map-link"  target="_blank" href="https://www.google.com/maps/place/Pixel6+Web+Studio+Pvt.+Ltd./@18.5312633,73.8427334,18z/data=!4m5!3m4!1s0x3bc2c07c427553b1:0xf926a2a068d70d09!8m2!3d18.5312684!4d73.8424707">-->
	</a>
</div>