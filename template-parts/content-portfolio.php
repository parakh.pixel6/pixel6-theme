<section class="clearfix px_container slideshow_wrap">
	<div class="px_container_top content_wrapper_width clearfix">
		<?php get_template_part('slideshow'); ?>
	</div>
	<div class="testimonial_sec clearfix content_wrapper_width">
		<?php $firstPosts =get_posts(array( 'posts_per_page' => 1,'post_type' => 'testimonial' ));  ?>
		<?php foreach($firstPosts as $firstPost){ ?>
		<div class="px_3coloumn">
			<blockquote class="px_service_quote px_whitebg_quote">
				<div class="px_author_sec">
					<?php if(has_post_thumbnail($firstPost->ID)){?>
					<?php echo get_the_post_thumbnail($firstPost->ID, array(45, 45)); ?>
					<?php } ?>
					<div class="auther_details">
						<p><?php echo apply_filters('the_excerpt', $firstPost->post_content); ?><span class="author_name">&dash; <?php echo get_the_author($firstPost->ID);?></span></p>
					</div>
				</div>
			</blockquote>
		</div>
		<?php }?>
		<?php 
			$client_args = array( 'post_type' => 'client', 'posts_per_page' => 5, 'order' => 'ASC');
			$clients = get_posts( $client_args );
		?>
		<div class="clients-wrapper clearfix">
			<h2>our clients</h2>
			<div class="client-block">
				<ul class="client-logos clearfix">
					<?php foreach ($clients as $key => $client) { ?>
						<li class="comapny-logo">
							<?php if(has_post_thumbnail($client->ID)){?>
								<?php echo get_the_post_thumbnail($client->ID); ?>
							<?php } ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="clearfix services_quote_wrapper">
		<?php 
			$args = array( 'post_type' => 'acme_quote', 'posts_per_page' => 3, 'orderby' => 'rand', 'order' => 'ASC');
			$posts_array = get_posts( $args );
			foreach ($posts_array as $key => $value) {
				$quote = $posts_array[$key];
				$user = $quote->post_author;
		?>
			<div class="px_3coloumn">
				<blockquote class="px_service_quote px_whitebg_quote">
					<div class="px_author_sec">
						<div class="quote-author-img">
						<?php if(has_post_thumbnail($quote->ID)){?>
							<?php echo get_the_post_thumbnail($quote->ID); ?>
						<?php } else{?>
							<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/user-placeholder.png">
							<?php }?>
						</div>
						<div class="auther_details">
							<?php echo $quote->post_title; ?>
							<p><small><?php echo $quote->post_content; ?></small></p>
						</div>
					</div>
				</blockquote>
			</div>
		<?php } ?>
		</div>
	</div>
</section>