<section class="clearfix px_container slideshow_wrap">
	<div class="testimonial_sec clearfix content_wrapper_width">
		<div class="clearfix services_quote_wrapper">
		<?php 
			$args = array( 'post_type' => 'career', 'posts_per_page' => -1, 'orderby' => 'rand', 'order' => 'ASC');
			$query  = new WP_Query( $args );
			 if ($query->have_posts() ) : 
				while ($query->have_posts() ) : $query->the_post(); 
					?>
				<?php if( get_field( 'job_status' )) : ?>
					<div class="px_3coloumn">
						<blockquote class="px_service_quote px_whitebg_quote">
							<div class="px_author_sec">
								<div class="career_auther_details">
									<strong><?php echo the_title(); ?></strong>
									<h4><small>
											<strong>Skills: </strong> <?php echo the_field("skills"); ?><br>
											<strong>Experience: </strong> <?php echo the_field("experience"); ?> Years<br>
											<strong>Job Description: </strong> <?php echo the_excerpt(); ?><br>
									</small></h4>
									<a href="<?php echo the_permalink(); ?>"><small>Read More>></small></a>
								</div>
							</div>
						</blockquote>
					</div>
					<?php
					else:
						echo ""; 
					endif; 
				endwhile;                                                            				
			endif;          
		    ?>
		</div>
	</div>
</section>
