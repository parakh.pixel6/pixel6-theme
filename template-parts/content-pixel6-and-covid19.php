<section class="clearfix px_container profile">

	<div class="px_container_top content_wrapper_width clearfix">

		<div class="px_1coloumn coloumn_first left-column">

			<div class="px_project_heading">

				<h2>Fight the battle</h2>

				<span>what you can do</span>

			</div>

			<div class="px_project_desc clearfix">

				<ul class="clearfix">
					<li>Get Vaccinated</li>
					
					<li>Social Distancing</li>

					<li>Stay At Home</li>

					<li>Wash Your Hands</li>

					<li>Work From Home</li>

					<li>Always Use Masks</li>

					<li>Help the Covid-19 fighters</li>

					<li>Support Daily Wage Workers</li>

					<li>Donate Generously</li>

					<li>Spread Awareness Not Hoaxes</li>

					<li>Learn &amp; Explore</li>

					<li>Know Your Environment</li>					

				</ul>				

			</div>

			

		</div>

		<div class="px_2combinecoloumn coloumn_last">

			<div class="px_project_heading">

				<h2><?php the_title();?></h2>

			</div>
			<?php while ( have_posts() ) : the_post(); ?> 
		        <div class="px_covid_desc">
		            <?php the_content(); ?>
		        </div>

		    <?php endwhile;?>

		</div>

	</div>

</section>

<script type="text/javascript">

	 jQuery(document).ready(function(){



	 	setTimeout(function(){

	 		jQuery('.team').imagesLoaded( { background: '.team-member' }, function() {

				jQuery('.team').masonry({

					itemSelector: '.team-member',

					isAnimated: true,

					isFitWidth: true

				});

			});

	 	}, 3500)

		

	});

</script>