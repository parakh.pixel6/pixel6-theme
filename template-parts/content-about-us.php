<section class="clearfix px_container profile">

	<div class="px_container_top content_wrapper_width clearfix">

		<div class="px_1coloumn coloumn_first left-column">

			<div class="px_project_heading">

				<h2>About Pixel6</h2>

				<span>who we are</span>

			</div>

			<div class="px_project_desc clearfix">

				<ul class="sidebar-link clearfix">

					<li><a href="#about">About Us</a></li>

					<li><a href="#vision">Vision</a></li>

					<li><a href="#mission">Mission</a></li>

					<li><a href="#capabilities">Capabilities</a></li>
					
					<li><a href="#careers">Careers</a></li>

<!-- 					<li><a href="#team">Team</a></li>
 -->				</ul>				




			</div>

			<div class="testimonial_sec clearfix">

				<?php 

				$args = array( 'post_type' => 'acme_quote', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC');

				$posts_array = get_posts( $args );

				foreach ($posts_array as $key => $value) {

					$quote = $posts_array[$key];

					$user = $quote->post_author;

				?>

					<div class="px_3coloumn">

						<blockquote class="px_service_quote px_whitebg_quote">

							<div class="px_author_sec">

								<div class="quote-author-img">

								<?php if(has_post_thumbnail($quote->ID)){?>

									<?php echo get_the_post_thumbnail($quote->ID); ?>

								<?php } else{?>

									<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/user-placeholder.png">

									<?php }?>

								</div>

								<div class="auther_details">

									<?php echo $quote->post_title; ?>

									<p><small><?php echo $quote->post_content; ?></small></p>

								</div>

							</div>

						</blockquote>

					</div>

				<?php } ?>



			</div>

		</div>

		<div class="px_2combinecoloumn coloumn_last">

			<div class="px_project_heading" id="about">

				<h2><?php the_title();?></h2>
				<span>what we do</span>

			</div>

			 <?php while ( have_posts() ) : the_post(); ?> 
		        <div class="px_project_desc">
		            <?php the_content(); ?>

		            <!-- <div class="px_2combinecoloumn coloumn_last">

						<div class="px_project_heading" id="team">
							<h2>THE TEAM</h2>
							<span>who we are</span>
						</div>

						<div class="px_project_desc">

							
						</div>

					</div>
 -->
		        </div>

		    <?php endwhile;?>
		</div>

	</div>

</section>

<script type="text/javascript">

	 jQuery(document).ready(function(){



	 	setTimeout(function(){

	 		jQuery('.team').imagesLoaded( { background: '.team-member' }, function() {

				jQuery('.team').masonry({

					itemSelector: '.team-member',

					isAnimated: true,

					isFitWidth: true

				});

			});

	 	}, 3500);

	 	//smooth scroll
				
		jQuery(".sidebar-link a[href^='#']").click(function(e) {
			e.preventDefault();
			
			var position = jQuery(jQuery(this).attr("href")).offset().top;

			jQuery("body, html").animate({
				scrollTop: position
			}, 1500);
		});
	});

</script>