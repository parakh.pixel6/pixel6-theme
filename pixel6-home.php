<?php /**
    Template Name: Home Page
 */
    get_header();
    ?>
<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="clearfix px_container banner-section">
				<div class="px_container_top content_wrapper_width clearfix">

				<?php
				 $firstPosts = get_posts(array( 'orderby' => 'rand',
				'posts_per_page' => 1,
				'post_type' => 'portfolio' ,
				 'portfolio_category'  => 'slideshow'
				  ));  ?>

				<div class="slides_wrappper">
					<div class="project_details_img"><img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/project_mberry.png" alt="slide1" /></div>
					<div class="px_slideshow">
						<?php $post_title = get_post_meta($post->ID,'theme_post_subtitle_value', true);
					 ?>
						<div class="slides slide_1 slideanimate">
							<div class="px_slideshow_left">
								<div class="px_post_heading">
									<h3 class="slide_title"><?php echo get_the_title($firstPosts[0]->ID);?></h3>
									<?php if(!empty($post_title)){ ?>
										<span class="slide_subtitle"><?php echo $post_title?></span>		
									<?php }?>					
								</div>
								<div class="px_post_content">
									<div class="post_content_slider">
										<?php echo apply_filters('the_excerpt', $firstPosts[0]->post_content); ?>
									</div>
									
								</div>
								<a href="<?php echo esc_url( home_url( '/' ) ).'portfolio'; ?>" class="white_button view_button clearfix">view our portfolio <span>&raquo;</span></a>
								
							</div>
							<?php if(has_post_thumbnail($firstPosts[0]->ID)){?>
							<div class="px_slideshow_right">
								<?php echo get_the_post_thumbnail($firstPosts[0]->ID); ?>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<div class="px_container_bottom">
		<div class="px_services_list content_wrapper_width clearfix">
			<article class="px_services px_3coloumn">
				<div><h2>UI Engineering</h2><span>front end is a big deal</span></div>
				<p class="service_details">UI/UX Development with cutting edge technologies from HTML5/CSS3/JS to the latest versions of Angular/Node/React and of course an unrivalled focus on Optimisation, SEO, x-Browser & Responsiveness.</p>
			</article>
			<article class="px_services px_3coloumn">
				<div><h2>Full Stack</h2><span>powerful brain & a healthy heart</span></div>
				<p class="service_details">Backend development with open source LAMP stack, flexible CMS's & robust eCommerce platforms for powerful, secure, reliable yet faster websites & web applications.</p>
			</article>
			<article class="px_services px_3coloumn">
				<div><h2>Mobile Apps</h2><span>smart phones are here to rule</span></div>
				<p class="service_details">Intuitive, fast and quick mobile apps with hybrid frameworks for iOS and Android phones for the mobile-first approach kind of businesses where time to market is the key.</p>
			</article>
			<?php $firstPosts =get_posts(array( 'posts_per_page' => 3,'post_type' => 'testimonial' ));  ?>
			<?php  foreach($firstPosts as $firstPost){ ?>
			<div class="px_3coloumn">
				<blockquote class="px_service_quote"><p><?php echo apply_filters('the_excerpt', $firstPost->post_content); ?><span class="author_name">&dash; <?php echo get_the_author();?></span></p></blockquote>
			</div>
			<?php }?>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>