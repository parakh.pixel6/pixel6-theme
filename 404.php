<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package pixel
 */

get_header(); ?>
					<div class="px_container_top  content_wrapper_width clearfix ">
						<div class="px_2combinecoloumn">
							<div id="primary" class="content-area">
								<main id="main" class="site-main">

									<section class="error-404 not-found">
										<header class="page-header test">
											<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'pixel' ); ?></h1>
										</header><!-- .page-header -->

										<div class="page-content">
											<p><?php _e( 'It looks like nothing was found at this location.', 'pixel' ); ?></p>
							<?php get_search_form(); ?>

										</div><!-- .page-content -->
									</section><!-- .error-404 -->

								</main><!-- #main -->
							</div><!-- #primary -->
						</div>
						<div class="px_1coloumn coloumn_last">
							<?php  get_sidebar('site-map'); ?>
						</div> 
					</div>
				</div>
				<?php get_footer(); ?>
