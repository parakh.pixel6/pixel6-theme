<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after
*
* @package pixel
*/
?>
			</div><!-- #content -->
			<footer class="px_footer clearfix">
				<div class="px_footer_wrapper content_wrapper_width">
					<div class="px_view clearfix">
						<div class="px_3coloumn">
							<div class="px_view_section">
								<h3 class="px_views_heading"><span class="px_mad_text">Pixel6</span>jobs</h3>
								<p>Interested in working at Pixel6 ?<br> If you are an enthusiastic BE/B.Tech or MCA/MCS, please submit your application.</p>
								<a href="https://pixel6.co/software-trainee-engineer/" class="px_visitblog_link">Click here to apply &nbsp;<i class="fa-solid fa-arrow-right"></i></a>
							</div>
						</div>

						<div class="px_3coloumn">
							<div class="px_view_section">
								<h3 class="px_views_heading">
									<span class="px_mad_text">Pixel6</span>SOCIAL
								</h3>
								<p>We are Social !<br>Follow us to discover our work, explore the vibrant culture, get to know our team, and uncover exciting opportunities.</p>
								<div class="social-link">
									<a class="px_visitblog_link social-link" href="https://www.linkedin.com/company/pixel6" target="_blank"><i class="fa-brands fa-linkedin"></i></a>
									<a class="px_visitblog_link social-link" href="https://goo.gl/maps/W745fEvgW5iNaW5o7" target="_blank"><i class="fa-brands fa-google"></i></a>
									<a class="px_visitblog_link social-link" href="https://twitter.com/pixel6web?lang=en" target="_blank"><i class="fa-brands fa-x-twitter"></i></a>
									<a class="px_visitblog_link social-link" href="https://github.com/thinkoverit/" target="_blank"><i class="fa-brands fa-github"></i></a>
									<a class="px_visitblog_link social-link" href="https://www.facebook.com/pixel6.co/" target="_blank"><i class="fa-brands fa-facebook"></i></a>
									<a class="px_visitblog_link social-link" href="https://www.instagram.com/pixel6.co" target="_blank"><i class="fa-brands fa-instagram"></i></a>
								</div>
							</div>
						</div>
						<div class="px_3coloumn">
							<div class="px_view_section px_contact_section">
								<a href="<?php echo home_url( '/contact/#request' ); ?>" class="request_quote">Get a free Quote <span>&raquo;</span></a>
								<p class="contact-email"><span>E-mail us </span>  /  <a>info@pixel6.co</a></p>
								<p class="contact-phonenum"><span>Phone</span>	/	<a href="tel:+918805060506">+91 88 05 06 05 06</a></p>
							</div>
						</div>
					</div>
					<div class="px_footer_bottom clearfix">
						<nav id="site-navigation-footer" class="clearfix px_footer_nav align_right"> <!-- role="navigation" -->
							<?php $walker = new Menu_With_Description; ?>
							<?php wp_nav_menu( array( 'theme_location' => 'footerArray', 'menu_id' => 'Menu 1') ); ?>
						</nav>
						<div class="copyright_section ">
							<span>&copy; <?php echo date('Y'); ?> Pixel6 Web Studio Pvt. Ltd.</span>
						</div>
					</div>
				</div>
			</footer><!-- #colophon -->
			<!-- #page -->
			<?php wp_footer(); ?>
		</section>
	</body>
</html>