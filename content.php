<?php
/**
 * @package pixel
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( is_author() ) { ?>
	<div class="px_service_quote px_whitebg_quote">
	<?php }?>
	<header class="entry-header">
		<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
	<?php if ( !is_single() ) { ?>
	<p><?php echo get_the_excerpt(); ?></p>
	<?php } else { ?>
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pixel' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>
<?php } ?>
	<?php if ( is_author() ) { ?>
	</div>
	<?php }?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'pixel' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	
</article><!-- #post-## -->