<?php
/**
 * The template for displaying search results pages.
 *
 * @package pixel
 */

get_header(); ?>
					<div class="px_container_top  content_wrapper_width clearfix ">
						<div class="px_2combinecoloumn">
							<section id="primary" class="content-area">
								<main id="main" class="site-main">
									<?php if ( have_posts() ) : ?>
										<?php /* Start the Loop */ ?>
										<?php while ( have_posts() ) : the_post(); ?>

											<?php
											/**
											 * Run the loop for the search to output the results.
											 * If you want to overload this in a child theme then include a file
											 * called content-search.php and that will be used instead.
											 */
											get_template_part( 'content', 'search' );
											?>

										<?php endwhile; ?>

										<?php the_posts_navigation(); ?>

									<?php else : ?>

										<?php get_template_part( 'content', 'none' ); ?>

									<?php endif; ?>
								</main><!-- #main -->
							</section><!-- #primary -->
						</div>
						<div class="px_1coloumn coloumn_last">
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
				<?php get_footer(); ?>
