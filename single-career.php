<?php
/**
 * The template for displaying all single posts.
 *
 * @package pixel
 */

get_header(); ?>
	<div class="px_container_top  single_container_top content_wrapper_width clearfix ">
		
		<div class="px_2combinecoloumn">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<?php while ( have_posts() ) : the_post(); ?>
					<p> <strong>Job Description: </strong> </p> <?php echo the_field("job_description"); ?><br>
					<p><strong>The Candidate need to be : </strong> </p> <?php echo the_field("candidate_profile"); ?><br>
					 <p><strong>Skills: </strong><?php echo the_field("skills"); ?></p><br>
					  <p><strong>Experience: </strong><?php echo the_field("experience"); ?> Years</p><br>
					  <p><strong>Qualification: </strong><?php echo the_field("qualification"); ?></p><br>
					  <p><strong>How to Apply: </strong><?php echo the_field("how_to_apply"); ?></p><br>
					<?php endwhile; // end of the loop. ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<div class="px_1coloumn coloumn_last">
			<div id="secondary" class="widget-area" role="complementary">
				<div class="coloumn_50">
					<div class="px_sidebar_details">
						<div class="px_sidebar_heading">
							<h3>Job Posts</h3>
							<span></span>
						</div>
						<div class="px_aside_desc">
							<ul>
							<?php
								$args = array( 'post_type' => 'career', 'posts_per_page' => -1, 'orderby' => 'rand', 'order' => 'ASC');
								$query  = new WP_Query( $args );
								 if ($query->have_posts() ) : 
									while ($query->have_posts() ) : $query->the_post();
										if(get_field( 'job_status' )) :
										?>
										<li><a href=" <?php the_permalink(); ?> "><?php echo the_title(); ?> </a> </li>
										<?php
										endif;									
									endwhile;                                                            				
								endif;
							?>
							</ul>
						</div><!-- #secondary -->

					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>