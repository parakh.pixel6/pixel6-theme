<?php
/**
 * The template for displaying all single posts.
 *
 * @package pixel
 */

get_header(); ?>
	<div class="px_container_top  single_container_top content_wrapper_width clearfix ">
		<div class="px_2combinecoloumn">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'single' ); ?>
					<?php endwhile; // end of the loop. ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<div class="px_1coloumn coloumn_last">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
