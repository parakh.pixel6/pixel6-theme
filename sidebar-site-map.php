<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package pixel
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
		<div class="px_sidebar_details">
			<div class="px_sidebar_heading">
				<h3>SITE MAP</h3>
				<span>Menu</span>
			</div>
			<div class="px_aside_desc">
				<ul>
				<?php
					$menu = wp_get_nav_menu_items('Menu 1');
					foreach( $menu as $item ){
                        echo '<li><a href="' . $item->url. '">' .   $item->title.'</a> </li> ';
					}
				?>
				</ul>
			</div><!-- #secondary -->
		</div>
</div>