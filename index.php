<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package pixel
*/
get_header(); ?>
<div class="px_container_top  content_wrapper_width clearfix ">
	<div class="px_2combinecoloumn">
		<div id="primary" class="content-area">
			<div class="px_project_heading">
				<h2>RECENT THOUGHTS</h2>
				<span>like ‘em or not</span>
			</div>
			<main id="main" class="site-main">
				<?php if ( have_posts() ) : ?>
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="px_service_quote px_whitebg_quote">
							<?php
								/* Include the Post-Format-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Format name) and that will be used instead.
							*/	?>				
							<?php get_template_part( 'content', get_post_format() ); ?>				
						</div>
					<?php endwhile; ?>
					<?php wp_pagination(); ?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
	<div class="px_1coloumn coloumn_last">
		<?php get_sidebar(); ?>
	</div>
</div>
</div>
<?php get_footer(); ?>